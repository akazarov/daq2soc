#!/bin/bash
# Install distro from an ISO onto a virtual disk image
# . runqemu-iso <distro>.iso

iso=$1
os=${iso%.iso}
size=15G

disk_img=$os.qcow2
efi_img=$os.efi

PATH=/home/akazarov/qemu/qemu-6.1.0/build:$PATH

qemu-img create -f qcow2 $disk_img ${size}
qemu-img create -f qcow2 $efi_img 64M

qemu-system-aarch64 \
    -cpu cortex-a53 -M virt -m 8192 -nographic -smp 8 -boot d \
    -drive if=pflash,format=raw,file=QEMU_EFI.img \
    -drive if=pflash,format=qcow2,file=${efi_img} \
    -device virtio-scsi-device \
    -device scsi-cd,drive=cdrom \
    -device virtio-blk-device,drive=hd0 \
    -drive file=${iso},id=cdrom,media=cdrom,if=none \
    -drive file=${disk_img},id=hd0,if=none
