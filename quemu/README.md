0. Download and build latest (6.1.0) qemu from [https://www.qemu.org/] e.g. in `$HOME/qemu/qemu-6.1.0`
Scripts here assume path to the binaries as  `$HOME/qemu/qemu-6.1.0/build`, modify as necessary. 

1. Get QEMU_EFI.img
```
cd <workdir>
wget http://snapshots.linaro.org/components/kernel/leg-virt-tianocore-edk2-upstream/latest/QEMU-AARCH64/RELEASE_GCC5/QEMU_EFI.img.gz
```
2. Get Centos Stream 8 AARCH64 image from CERN
```
wget http://linuxsoft.cern.ch/centos/8-stream/isos/aarch64/CentOS-Stream-8-aarch64-latest-dvd1.iso
```

3. Run installer.sh with that image as parameter. This will create a virtual disk (15 GB, change this if needed) CentOS-Stream-8-aarch64-latest-dvd1.qcow2 and install CentOS on it.
```
> ./installer.sh CentOS-Stream-8-aarch64-latest-dvd1.iso
```
Create a local user `<localuser>`.

4. Run it from this disk. By default it will ask 8GB of virtual RAM and 8 CPU cores. You need a server-level host go get this.
> ./runemu.sh CentOS-Stream-8-aarch64-latest-dvd1.qcow2

5. Get loging prompt, or/and ssh to `<localuser>@localhost -P 2222`. The `8080` (nginx) port is exposed to `8888` on the host (change this in the script if needed).

6. Compile nginx and daq2soc module, install, run, connect from host like
```
curl -i -X HEAD 'localhost:8888/tdaq/probe'
```
