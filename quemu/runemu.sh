#!/bin/bash
# Boot distro from a QCOW2 or raw image
# . runqemu-img <distro>.qcow2

disk_img=$1
efi_img=${1%.*}.efi

PATH=/home/akazarov/qemu/qemu-6.1.0/build:$PATH

qemu-system-aarch64 \
    -cpu cortex-a53 -M virt -m 8192 -nographic -smp 8 \
    -drive if=pflash,format=raw,file=QEMU_EFI.img \
    -drive if=pflash,format=qcow2,file=${efi_img} \
    -device virtio-blk-device,drive=hd0 \
    -drive file=${disk_img},id=hd0,if=none  \
     -net user,hostfwd=tcp::2222-:22,hostfwd=tcp::8888-:8080 \
     -net nic
