/**
 * Defines class to construct an instance of a user class derived from DAQRequestProcessor from a dynamically loaded .so library
 */

#include <stdexcept>
#include <dlfcn.h>
#include <memory>
#include <stdexcept>
	
#include "DAQRequestProcessor.hpp"

using namespace tdaq::daq2soc ;

using DAQRequestProcessor_creator_t = DAQRequestProcessor *(*)() ;

class UserFactory {
public:
    explicit UserFactory(const char* library) {
        handler = dlopen(library, RTLD_NOW) ;
        if ( !handler ) {
            throw std::runtime_error(dlerror()) ;
        }
        dlerror() ;
        creator = reinterpret_cast<DAQRequestProcessor_creator_t>(dlsym(handler, "create")) ;
        if ( creator == NULL ) {
            const char * dlsym_error = dlerror() ;
            if ( dlsym_error ) {
                throw std::runtime_error(dlsym_error) ;
            }   
        }
    }

    std::unique_ptr<DAQRequestProcessor> create() const {
        return std::unique_ptr<DAQRequestProcessor>(creator()) ;
    }

    ~UserFactory() {
        if ( handler ) {
            dlclose(handler) ;
        }
    }

private:
    void* handler = nullptr ;
    DAQRequestProcessor_creator_t creator = nullptr ;
};

