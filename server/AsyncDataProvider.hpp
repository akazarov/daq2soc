#ifndef DAQ2SOC_ASYNC_DATA_PROVIDER_HPP
#define DAQ2SOC_ASYNC_DATA_PROVIDER_HPP

#include <map>
#include <vector>
#include <string>
#include <future>
#include <memory>
#include <chrono>
#include <thread>
#include <mutex>

#ifdef DEBUGME
#include <iostream>
#include <iomanip>
#include <experimental/source_location> 
static std::mutex mx ; 
#define DBG(msg) { std::time_t ts = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); \
std::lock_guard<std::mutex> lock(mx) ; \
const std::experimental::source_location loc = std::experimental::source_location::current() ;\
std::cerr << std::put_time(gmtime(&ts), "%Y-%m-%d %H:%M:%S") << " @" << loc.function_name() << \
"[" << loc.file_name() << ":" << loc.line() << "] " << msg << std::endl ; } ;
#else
#define DBG(msg)
#endif

/**
 * @file AsyncDataProvider.hpp
 * 
 * Declares tdaq::daq2soc::AsyncDataProvider class, a base class for tdaq::daq2soc::DAQRequestProcessor.
 */

namespace tdaq {
namespace daq2soc {

// TODO share with client in a common header?
/**
 * @typedef data_t
 * Type of data returned to client in @ref tdaq::daq2soc::Sender::getDataAsync()
 */
using data_t = std::vector<uint8_t> ;

using promised_data = std::tuple<std::string, std::shared_ptr<data_t>> ;

/**
 * @class AsyncDataProvider
 * @brief allows to asynchronously submit some data
 * 
 * Interface class defining methods allowing user to asynchronously submit some data and thus to push it back to the client.
 * User class must implement @ref AsyncDataProvider::processAsyncRequest() method which is executed in a sepatate thread, launched at the instantiation time.
 * 
 * @ref DAQRequestProcessor inherits from this class, adding @ref tdaq::daq2soc::DAQRequestProcessor::processSyncRequest() virtual method to the interface.
 */
class AsyncDataProvider {

friend class HTTPSession ;
friend class HTTPServer ;
friend class HTTPServerSSL ;
friend class HTTPSessionSSL ;

	private:

	std::mutex m_mtx ;
	std::promise<bool> 	m_stop_p ;
	std::future<bool> 	m_stop_f ; // flag to stop async thread

	std::promise<promised_data> 		m_data_ready_p ; // promise/future to sync and wait for a data to be ready, holding data ID submitted by user
	std::shared_future<promised_data> 	m_data_ready_f ;  

	std::thread m_thread ;

	// starts the user thread, called from HTTPServer
	void start() ;

	// blocks (wait_for) on m_data_ready_f
	// called from long polling http request handler (in HTTPSession)
	// empty values returned on timeout, ready data {ID,data_t} returned if data is available
	// may be called from multiple requests/threads: data to be copied to all requesters
	std::tuple<std::string, std::shared_ptr<data_t>>
	waitForAsyncData(std::chrono::seconds timeout) ;

	public:

	AsyncDataProvider() ;

	AsyncDataProvider(AsyncDataProvider&&) = delete ;
	AsyncDataProvider(const AsyncDataProvider&) = delete ;
	AsyncDataProvider& operator=(const AsyncDataProvider&) = delete ;

	~AsyncDataProvider() ;

	/// may be called from user thread (@ref processAsyncRequest) to block for a certain time interval
	/** 
	 *  Use it as a blocker function and also as the framework signal to return from the user thread
	 * 
	 *	@param timeout time to block in milliseconds
	 *	@return \c true if user function @ref processAsyncRequest has to exit (typically at the exit of the program execution), \c false otherwise
     *
	 *	@see tdaq::daq2soc::UserExample::processAsyncRequest for example of use
	 */
	bool block(std::chrono::milliseconds timeout) ;

	/// should be called from user thread (@ref processAsyncRequest) to check if it is time to exit
	/**
	 *  Call it periodically and if it returns true, return from the function. It means, the application is exiting and thread shutdown is expected.
	 *  A shortcut for block(0). You can use @ref block for blocking and checking for ther signal in one call.
	 * 
	 * @return true if user function @ref processAsyncRequest has to exit (typically at the exit of the program execution), false otherwise
	 */
	bool shall_exit() {
		return block(std::chrono::milliseconds(0)) ;
	} 

	/// Pure virtual (interface) function that is executed in a separate thread by the daq2soc library
	/**
	Must be implemented by the user. Typical use of it is to call @ref fulfillAsyncRequest to submit new data for async client requests.
	For blocking purposes (as alternative to yielding), user should periodically call @ref block and if it returns true the function must return, e.g.
	@code
	while (true ) {	
		// do something, possibly publish user data with @ref fulfillAsyncRequest
		// then yield and continue, return from processAsyncRequest only when @ref block returns true
		if ( block(std::chrono::milliseconds(500)) ) return ;
	}
	@endcode

	@see UserExample#processAsyncRequest() as example of implementation
	*/ 
	virtual void processAsyncRequest() = 0 ;
 
	/// Should be called from user thread @ref processAsyncRequest when you want to asynchronously publish data with some ID
	/**
	@param id some ID for the published data, that can be used in async request from the client side (@ref tdaq::daq2soc::Sender::getDataAsync)
	@param ready_data unique_ptr to data_t - user data that will be sent to the client
	@return bool indicating that there were no errors (when true). At the moment can be ignored by user code.
	*/
	bool fulfillAsyncRequest(const std::string& id, std::unique_ptr<data_t> ready_data) ;

	/**
	 * @brief Should be called from asynchronous user thread @ref processAsyncRequest when you want to asynchronously publish data with some ID
	 * 
	@param id some ID for the published data, that can be used in request from the client side
	@param ready_data a (JSON) string with user data that us converted to \c unique_ptr<data_t>
	@return bool indicating that there were no errors (when true). At the moment can be ignored by user code.
	*/ 
	bool fulfillAsyncRequest(const std::string& id, const std::string& ready_data) {
		return fulfillAsyncRequest(id, std::make_unique<data_t>(std::begin(ready_data), std::end(ready_data))) ;
	}
} ;

}} // tdaq::daq2soc

#endif