#ifndef DAQ2SOC_HTTP_SERVERSSL_HPP
#define DAQ2SOC_HTTP_SERVERSSL_HPP

#include <string>
#include <chrono>
#include <thread>
#include <vector>
#include <memory>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl/stream.hpp>

#include "DAQRequestProcessor.hpp"
#include "HTTPServer.hpp"

namespace ssl = boost::asio::ssl ; 

namespace tdaq {
namespace daq2soc {

/// Internal class, not exposed to daq2soc user API.
/**
 * Implementation of HTTPSession for SSL stream.
 */
class HTTPSessionSSL: public HTTPSession, public std::enable_shared_from_this<HTTPSessionSSL> {

public:
    HTTPSessionSSL(tcp::socket socket, ssl::context& context, std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& domain) ;
    ~HTTPSessionSSL() ;

protected:
    // The SSL socket for the currently connected client.
    ssl::stream<tcp::socket> m_socket ;

    // implementation for SSL socket
	void readRequest() override ;
    void writeResponse() override ;
} ;


/**
 * @brief Class that can be instantiated in a user application to access daq2soc server functionality over SSL layer.
 * 
 * @see tdaq::daq2soc::HTTPServer for non-SSL variant of this class
 */
class HTTPServerSSL: public HTTPServer {

private:

    ssl::context                m_ssl_ctx ;

    std::string get_password() const { return "test" ; } ;

	// accept SSL connection and start session, called from HTTPServer::start()
	void acceptStartSession(tcp::socket& socket) override ;
    
public: 
	HTTPServerSSL(  std::shared_ptr<DAQRequestProcessor> daq_proc,
                    const std::string_view& address = defaults::address_ip4,
                    short unsigned port = defaults::port_ssl,
                    const std::string_view& domain = defaults::domain) ;

	~HTTPServerSSL() ;
} ;

}}

#endif
