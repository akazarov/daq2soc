#include <stdexcept>
#include <cstdlib>
#include <iostream>

#include "HTTPServerSSL.hpp"
#include "certs/certificates.hpp"

namespace tdaq::daq2soc {

HTTPServerSSL::HTTPServerSSL(std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& address, short unsigned port, const std::string_view& domain):
HTTPServer(proc, address, port, domain),
m_ssl_ctx(boost::asio::ssl::context::sslv23)
{

    m_ssl_ctx.set_options(
        boost::asio::ssl::context::default_workarounds
        | boost::asio::ssl::context::no_sslv2
        | boost::asio::ssl::context::single_dh_use) ;

    m_ssl_ctx.set_password_callback(std::bind(&HTTPServerSSL::get_password, this)) ;

	// load from files // TODO get file pathes from environment?
    //m_ssl_ctx.use_certificate_chain_file("certs/cert.pem") ;
    //m_ssl_ctx.use_private_key_file("certs/key.pem", boost::asio::ssl::context::pem) ;
    //m_ssl_ctx.use_tmp_dh_file("certs/dh.pem") ;

	// load from code
    m_ssl_ctx.use_certificate_chain(
        boost::asio::buffer(tdaq::daq2soc::server::cert.data(), tdaq::daq2soc::server::cert.size())) ;

    m_ssl_ctx.use_private_key(
        boost::asio::buffer(tdaq::daq2soc::server::key.data(), tdaq::daq2soc::server::key.size()),
        boost::asio::ssl::context::file_format::pem) ;

    m_ssl_ctx.use_tmp_dh(
        boost::asio::buffer(tdaq::daq2soc::server::dh.data(), tdaq::daq2soc::server::dh.size())) ;
}

HTTPServerSSL::~HTTPServerSSL() {
DBG("HTTPS server stopped")
}

void HTTPServerSSL::acceptStartSession(tcp::socket& socket) {
	m_acceptor.async_accept(socket, [&](beast::error_code ec) {
DBG("Got HTTPS connection, starting session")			
        if( !ec ) {
			std::make_shared<HTTPSessionSSL>(std::move(socket), m_ssl_ctx, m_processor, m_domain)->start() ; // handle the connection in separate http session
		}    	
		acceptStartSession(socket) ; // keep accepting
    }) ;
}

}
