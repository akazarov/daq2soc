/**
	@file server_main.cpp
	@brief Main function of daq2soc server binary
 */

#include <cstdlib>
#include <iostream> 

// implementation of the HTTP layer, need to link with libdaq2soc_server.so
#include "HTTPServerSSL.hpp"

// load user class from a .so library
#include "UserFactory.hpp"

int
main(int argc, char* argv[]) {

if ( argc != 5 ) {
	std::cerr << "Usage: " << argv[0] << " <address> <port> <domain> <user_lib>\n" ;
	std::cerr << "  For IPv4:\n" ;
	std::cerr << "    " << argv[0] << " 0.0.0.0 8443 tdaq libuser.so\n" ;
	std::cerr << "  For IPv6:\n" ;
	std::cerr << "    " << argv[0] << " 0::0 8443 tdaq libuser.so\n" ;
	return EXIT_FAILURE;
}

auto port = static_cast<unsigned short>(std::atoi(argv[2])) ;
if ( !port ) {
	std::cerr << "Error: failed to read port number from " << argv[2] << std::endl ;
	return EXIT_FAILURE ;
}

try {
DBG("Contructing HTTPS server")
	tdaq::daq2soc::HTTPServerSSL server(std::make_shared<UserFactory>(argv[4])->create(), argv[1], port, argv[3]) ;
	server.start() ;

DBG("Waiting for termination")
	server.wait() ;
} catch (std::exception const& e) {
	std::cerr << "Error: " << e.what() << std::endl ;
	return EXIT_FAILURE ;
}
DBG("Exiting daq2soc HTTP server")
}