#include <boost/asio/ssl/error.hpp>

#include "HTTPServerSSL.hpp"

namespace tdaq::daq2soc {

HTTPSessionSSL::HTTPSessionSSL(tcp::socket socket, ssl::context& context, std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& domain)
:HTTPSession(proc, domain), m_socket{std::move(socket), context} {
try {
	m_socket.handshake(boost::asio::ssl::stream_base::server) ; // throw
DBG("SSL handshake done")
	} catch ( std::exception& ex) { // no rethrow exception here, fail later // TODO proper error reporting to stderr
DBG("SSL handshake failed: " << ex.what())
	}
DBG("HTTPS session constructed") ;
}

HTTPSessionSSL::~HTTPSessionSSL() {
//	m_socket.shutdown() ; // connection closed, SSL shutdown
DBG("HTTPS session destroyed") ;
}

// Asynchronously receive a complete request message.
void
HTTPSessionSSL::readRequest() {
	auto self = shared_from_this();

DBG("reading HTTPS request")
	http::async_read(
		m_socket, m_buffer, m_request,
		[self](beast::error_code ec, std::size_t bytes_transferred) {
			boost::ignore_unused(bytes_transferred) ;
			if ( !ec ) {
//DBG("reading OK, processing")
				self->processRequest() ; 
			} else
			if ( ec == http::error::end_of_stream || ec == boost::asio::ssl::error::stream_truncated ) {
DBG("OK, client closed stream, just exit from this session and close connection")
			} else {
DBG("failure in reading request: " << ec.message())
			}
		}) ;
//DBG("exiting reading http request")
}

void
HTTPSessionSSL::writeResponse() {
	auto self = shared_from_this();

DBG("writing response")
	//m_response.content_length(m_response.body().size());
	m_response.prepare_payload() ;
	http::async_write(
		m_socket, m_response,
		[self](beast::error_code ec, std::size_t) {
DBG("HTTPS response sent. Request keep alive: " << self->m_request.keep_alive())

			if ( !self->m_request.keep_alive() ) {
DBG("no keep-alive requested, shutting down connection")	// what to do in SSL?
				// self->m_socket.shutdown(tcp::socket::shutdown_send, ec) ;
				try {
					self->m_socket.shutdown() ; // connection closed, SSL shutdown
				} catch ( const std::exception& ex ) {
DBG("exception in ssl socket shutdown: " << ex.what())
				}
			} else {
				// read next request
DBG("continuing reading requests in the same HTTPS session")	
				self->readRequest() ;
			}
		}) ;
}
}