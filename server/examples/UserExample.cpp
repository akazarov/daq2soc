#include <unistd.h>
#include <cstdint>
#include <cstdlib>
#include <string> 
#include <cstring> 
#include <map> 
#include <vector> 
#include <fstream> 
#include <iostream> 
#include <iterator>
#include <chrono>
#include <algorithm>
		
#include "UserExample.hpp"

// header with data2json implementation header, part of the package
// you need to include it if you want to use data2json helper functions for serialization of your data.
// C++17 required here
#include "common/cpp/json/data2json.hpp"

// definition of histogram as tuple
#include "common/cpp/Histo.hpp"

/**
 * @example UserExample.cpp
 * 
 * Defines example of the user code for handling daq2soc requests on the server side.
 * This includes implementation of two member functions of the base classes: for running code in a separate thread
 * and for processing a client synchronous request.
 *
 * @note The following piece of code must be present in user library so the server library can dynamically load user class implementation.
 * 
 * @code

extern "C" {
DAQRequestProcessor* create() {
    return new UserExample();
} }
 * 
 * @endcode
 * 
 * 
 * @see tdaq::daq2soc::DAQRequestProcessor()
 * 
 * Presently the implementation contains some example code and can/should be fully reimplemented by users.
 * The code of this file is loaded by daq2soc server from libUserExample.so library.
 */
namespace tdaq {
namespace daq2soc {

// needed of you want user class to be loaded by the precompiled server binary from user .so library 
extern "C" {
DAQRequestProcessor* create() {
    return new UserExample();
}
}

// an example of returned data, default value
const uint8_t response[] = "HELLO FROM SOC\n" ;

/**
 * @brief Constructor. Initialize your attributes here.
 * 
 * A single instance of UserData class is created when daq2soc server is started.
 */
UserExample::UserExample() {
	std::cerr << "UserExample constructed\n";

	counter = 1 ;
}

UserExample::~UserExample() { 
	std::cerr << "UserExample destructed\n";
// deallocate user structures
} 

/**
 * @brief User function (example) to execute code in separate thread.
 * 
 * This function should be fully customized by users. The code is executed independently from other user (http) requests.
 * Handling of concurrent access to class members need to be implemented by users.
 * Call @ref fulfillAsyncRequest() for publishing your data asynchronously.
 * It is assumed that this function never exits and runs until program termination. To check that you need to exit, call @ref block() 
 * and if it returns 'true', do return. 
 */ 
void UserExample::processAsyncRequest() {
std::cerr << "Started user thread\n";
	while ( !block(std::chrono::milliseconds(500)) ) {
		// do something useful: update histogram
		// then pause and loop

		counter++ ;
		fulfillAsyncRequest("counter", tdaq::daq2soc::data2json(counter.load())) ;

		if ( counter % 10 == 0 ) {
			try {
				example::Histogram<1024> hist { "random historgram 1024 bins", {} } ;
				std::for_each(std::begin(std::get<1>(hist)), std::end(std::get<1>(hist)), [](float& x) { x = (float)std::rand()/RAND_MAX ;}) ;
				fulfillAsyncRequest("histogram1024", tdaq::daq2soc::data2json(hist)) ;
			} catch ( const std::exception& ex ) {
				std::cerr << ex.what() ;
				// fulfillAsyncRequest("histogram1024", std::current_exception() ) ;
			}
		}
		// to check that you need to exit without blocking
		if ( block(std::chrono::milliseconds(0)) ) { return ; }
	}
std::cerr << "Exiting from user thread!\n" ;
}

/**
 * @brief Example of user code to processing a request received from daq2soc client. Impelementatin of DAQRequestProcessor::processSyncRequest().
 * 
 * This function should be fully customized by users.
 * The current example implementation dumps the request to a log file in /tmp and also handles few commands (endpoints):
 * - get_counter: returns a value increasing counter (this is an example attribute of this user class), possibly as JSON.
 * - get_load:  returns average load from the system as a string
 * - rc: gets the RC command name as parameter (does nothing)
 * - sleep: calls system sleep function for the number of seconds passed as command parameter 'value'
 * - get_histogram: returns a Histogram tuple defined in common/cpp/Histo.hpp as JSON string (see client/cpp/examples/test_sync.cpp for decoding it back to C++ structure)
 * 
 * The user function gets and returns arbitrary user data as a vector of bytes, which can be either binary data (copy of a buffer, see line 104), 
 * conntent of a file (see lines 150-151) or a string (e.g. integer passed as string, see lines 169 and 167). For simplicity and interoperability 
 * (think of a JS application in a web browser), formatting the return value(s) as JSON structure is recommended. A helper function data2json is provided to convert a wide range of types (like a tuple) to JSON.
 *
 * @param endpoint a string defining a command, part of URL request to \c "/tdaq/<endpoint>" location on the server
 * @param parameters a map of name:value pairs of parameters passed from a client (passed as http request parameters \c "/tdaq/<endpoint>?name1=value1&name2=value2" etc)'
 * @param data_in vector of bytes received as payload from the client
 * @return request processing status (0 if success) of the command processing and possible payload 
 * 
 * @see tdaq::daq2doc::data2json() 
 */
std::tuple<int, std::vector<uint8_t>>
UserExample::processSyncRequest(
	const std::string& endpoint,
	const std::map<std::string, std::string>& parameters,
	const std::vector<uint8_t>& data_in) {

std::string out_filename = "/tmp/daq2soc.out" ;

// find parameter 'release' passed from client
auto release = parameters.find("release") ;
if ( release != parameters.end() ) {
	out_filename = "/tmp/" ;
	out_filename.append(release->second).append(".daq2soc.out") ;
}

std::ofstream outf(out_filename, std::ios_base::app) ;

// a return structure, initialize with 0 status and fill in the payload from response[] array, just as example
std::tuple<int, std::vector<uint8_t>> ret { 0, { std::begin(response), std::end(response) } };

// in case of error, fill in ret with error description
if ( !outf.good() ) {
	std::string message = "Failed to open file " ;
	message.append(out_filename).append(" for append: ").append(strerror(errno)).append("\n") ;
	std::get<1>(ret) = { std::begin(message), std::end(message) } ;
	std::get<0>(ret) = 1 ; // some error code, meaningful for client
	return ret ;
}

outf << "User function called with command: " << endpoint << "/";
if ( parameters.size() ) {
	outf  << "Parameters: " << endpoint << "/";
}
for ( auto p: parameters ) {
	outf << p.first << "=" << p.second << "; ";
}
outf << std::endl ;
if ( !data_in.empty() ) {
	outf << "Data: " << std::endl ;
	outf.write((char*)&data_in[0], data_in.size()*sizeof(uint8_t)) ;
	outf << std::endl ;
}

outf.close() ;

// for example, a call was tdaq/sleep?value=5
// the passed command is 'sleep', implementation: call system sleep() for the parameters['value'] seconds
auto seconds = parameters.find("value") ;
if ( endpoint == "sleep" && seconds != parameters.end() ) {
	int secs, slept;
	try {
		secs = std::stoi(seconds->second) ; // handle errors...
		slept = sleep(secs) ;
	} catch ( const std::exception& ex ) {
		std::string err{"Failed to convert: "} ;
		err.append(ex.what()) ;
		out_data_t ret { 1, { std::begin(err), std::end(err) }} ; // error code and payload
		return ret ;
	}
	if ( slept != 0 ) {
		std::get<0>(ret) = 1 ; // something went wrong
	}
	return ret ;
}

// handle RC command, passed as <host>/tdaq/rc?command=INITIALIZE
auto command = parameters.find("command") ;
if ( endpoint == "rc" && command != parameters.end() ) {
	// do something useful
	std::get<0>(ret) = 0 ; // command processes OK
	return ret ;
}

// return content of /proc/loadavg
if ( endpoint == "get_load" ) {
	std::ifstream file("/proc/loadavg") ;
	std::vector<uint8_t> ret_data(std::istream_iterator<uint8_t>{file}, std::istream_iterator<uint8_t>{}) ;
	std::get<1>(ret) = ret_data ;
	return ret ;
}

// resets the counter
if ( endpoint == "reset_counter" ) {
	counter = 0 ;
	return ret ;
}

// return increasing local counter (here encoded as string, a better and more generic alternative is to use JSON)
if ( endpoint == "get_counter" ) {
	auto format = parameters.find("format") ;
	std::string cnt ;
	if ( format != parameters.end() && format->second == "JSON" ) {
		cnt = tdaq::daq2soc::data2json(counter++) ;
	} else {
		cnt = std::to_string(counter++) ;
	}
	std::vector<uint8_t> ret_data(std::begin(cnt), std::end(cnt)) ;
	std::get<1>(ret) = ret_data ;
	return ret ;
}

// return randomly filled histogram
if ( endpoint == "get_histogram" ) {
	
	example::Histogram<10> hist { "random historgram", {} } ; // 
	// fill with random numbers
	std::for_each(std::begin(std::get<1>(hist)), std::end(std::get<1>(hist)), [](float& x) { x = (float)std::rand()/RAND_MAX ;}) ;
	// convert to json
	std::string json = tdaq::daq2soc::data2json(hist) ;
	std::vector<uint8_t> ret_data(std::begin(json), std::end(json)) ;
	std::get<1>(ret) = ret_data ;
	return ret ;
}

if ( endpoint == "get_histogram_1MB" ) {
	
	example::Histogram<100*1024> hist { "random historgram 1MB", {} } ; // 
	// fill with random numbers
	std::for_each(std::begin(std::get<1>(hist)), std::end(std::get<1>(hist)), [](float& x) { x = (float)std::rand()/RAND_MAX ;}) ;
	// convert to json
	std::string json = tdaq::daq2soc::data2json(hist) ;
	std::vector<uint8_t> ret_data(std::begin(json), std::end(json)) ;
	std::get<1>(ret) = ret_data ;
	return ret ;
}

// for test purposes, illustrate how to report bad result
if ( endpoint == "bad_request" ) {
	std::string_view json{ (const char*)data_in.data(), data_in.size()} ; // we assume it is JSON
	// auto data_in_tuple = tdaq::daq2soc::json2data<std::string, int, std::array<int,6>>(view) ;
	auto data_in_tuple = tdaq::daq2soc::json2data<int, unsigned int, long long, float, double, long double, std::string, std::string, std::array<float, 3>>(json) ;
	std::cerr << "got tuple data, first element: " << std::get<0>(data_in_tuple) ;
	return { 1, {} } ;
}

// throw an exception that will be rethron in the client by Sender::SendCommandSync
if ( endpoint == "get_exception" ) {
	throw std::runtime_error("Bad things happen, it's your turn to handle it now") ;
}

return ret ;
}

} }
