/**
 * @example standalone_example_main.cpp
 * 
 * Example of a daq2soc server standalone application. In this way you can integrate the HTTPServer functionality to your own binaries.
 * To build it, see user_server target in the Makefile.
 */

#include <cstdlib>
#include <iostream> 

// implementation of the HTTP layer, binary need to link with libdaq2soc_server.so
#include "HTTPServer.hpp"

// user class implementing daq2soc server API, link the binary with UserExample.o or dynamically with your library
#include "UserExample.hpp"

int
main(int argc, char* argv[]) {

if(argc != 3) {
	std::cerr << "Usage: " << argv[0] << " <address> <port>\n";
	std::cerr << "  For IPv4, try:\n";
	std::cerr << "    server_ex 0.0.0.0 8080\n";
	std::cerr << "  For IPv6, try:\n";
	std::cerr << "    server_ex 0::0 8080\n";
	return EXIT_FAILURE;
}

unsigned short port = static_cast<unsigned short>(std::atoi(argv[2]));

try {
std::cerr << "Contructing HTTP server" << std::endl ;
	// create an instance of user class and pass it to the server constructor
	// constructing the server starts the request processing cycle - no need to call run() unless you need to block the main thread
	// If you want to stop processing, just destroy the instance of HTTPServer 
	tdaq::daq2soc::HTTPServer server(std::make_shared<tdaq::daq2soc::UserExample>(), argv[1], port) ;
	server.start() ; // start accepting connections

std::cerr << "Waiting for termination" << std::endl ;
	server.wait() ;
	// blocks the main thread, waits for a process termination signal - no need to call if you don't need to block.
} catch (std::exception const& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE ;
}
std::cerr << "Exiting daq2soc HTTP server" << std::endl ;
}