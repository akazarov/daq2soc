#include <cstdint>
#include <tuple>
#include <vector>
#include <map>
#include <string>
#include <thread>
#include <atomic>

#include "DAQRequestProcessor.hpp"

/**
 * @example UserExample.hpp
 * 
 * Declares @ref tdaq::daq2soc::UserExample class and the virtual methods of the base classes.
 */
namespace tdaq {
namespace daq2soc {

/**
 * @class UserExample
 * @brief An example of @ref tdaq::daq2soc::DAQRequestProcessor interface extended by a user
 * 
 * Define here needed attributes and initialize them in @ref UserExample() construtor.
 */
class UserExample: public DAQRequestProcessor {

private:
std::atomic<uint64_t> counter ; // just an example of a user data

public:

UserExample() ;

/// @warning destructor must be virtual
virtual ~UserExample() ;

/**
 * Implementation of @ref AsyncDataProvider#processAsyncRequest().
 * Started by daq2soc server in a separate thread.
 */
virtual void processAsyncRequest() override ;

/**
 * Implementation of @ref DAQRequestProcessor#processSyncRequest().
 * Processes http requests from daq2soc client and returns some result.
 */
virtual
std::tuple<int, std::vector<uint8_t>>
processSyncRequest(
	const std::string& endpoint,
	const std::map<std::string, std::string>& parameters,
	const std::vector<uint8_t>& data_in) override ;

} ;

} }
