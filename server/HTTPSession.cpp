#include <boost/asio/error.hpp>

#include "HTTPServer.hpp"

namespace tdaq::daq2soc {

HTTPSessionTCP::HTTPSessionTCP(tcp::socket socket, std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& domain)
:HTTPSession(proc, domain), m_socket{std::move(socket)}
{}

HTTPSession::HTTPSession(std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& domain)
:m_processor{proc}, m_domain(domain)
{}

HTTPSessionTCP::~HTTPSessionTCP() {
	DBG("HTTP session destroyed") ;
}

HTTPSession::~HTTPSession() {
}

// Initiate the asynchronous operations associated with the connection.
void
HTTPSession::start() {
DBG("HTTP session started in thread " << std::this_thread::get_id())			
	readRequest() ;
}

// Asynchronously receive a complete request message.
void
HTTPSessionTCP::readRequest() {
	auto self = shared_from_this();

DBG("reading http request")
	http::async_read(
		m_socket, m_buffer, m_request,
		[self](beast::error_code ec, std::size_t bytes_transferred) {
			boost::ignore_unused(bytes_transferred) ;
			if ( !ec ) {
//DBG("reading OK, processing")
				self->processRequest() ; 
			} else
			if ( ec == http::error::end_of_stream ) {
DBG("OK, client closed stream, just exit from this session and close connection")
			} else {
DBG("failure in reading request: " << ec.message())
			}
		}) ;
//DBG("exiting reading http request")
}

// Determine what needs to be done with the request message.
void
HTTPSession::processRequest() {

	m_response = {} ;

	m_response.version(m_request.version()) ;
	m_response.keep_alive(m_request.keep_alive()) ;
	m_response.set(http::field::server, "SOC") ;

	switch( m_request.method() ) {
		case http::verb::head:
			m_response.result(http::status::ok) ;
			break ;
		case http::verb::subscribe:
			if ( processAsyncRequest() ) { // polling
				m_response.result(http::status::ok) ;
			} else {
				m_response.result(http::status::no_content) ; // return empty result, client continue polling
			}; 
			break ;
		case http::verb::get:
		case http::verb::post:
			if ( m_request.target().starts_with(m_domain.data())) {
				m_response.result(http::status::ok) ;
				createResponse();
				break;
			} else {
				m_response.result(http::status::not_acceptable);
				m_response.set(http::field::content_type, "text/plain") ;
				std::string msg("Request URL '" + std::string(m_request.target()) + "' not handled") ;
				m_response.body() = { msg.begin(), msg.end() } ;
				break;
			}

		default:
			m_response.result(http::status::bad_request);
			m_response.set(http::field::content_type, "text/plain") ;
			std::string msg( "Invalid request-method '" + std::string(m_request.method_string()) + "'") ;
			m_response.body() = { msg.begin(), msg.end() } ;
			break;
	}

DBG("writing response")
	writeResponse() ;
}

bool
HTTPSession::processAsyncRequest() {
 
DBG("Waiting for async data in thread " << std::this_thread::get_id())

	auto [id, data] = m_processor->waitForAsyncData(std::chrono::seconds(5)) ; // TODO get from a header	
DBG("got async data with ID: " << id)
	m_response.set("X-daq2soc-async-id", id) ;
	m_response.body() = *data.get() ; // copy or move?

	if ( !id.empty() ) {
DBG("returning async data with ID: " << id << " and size: " << data.get()->size())
		return true ;
	} else { // timeout, no data available yet
DBG("no data available, empty return")
		return false ;
	}
}

void
HTTPSession::createResponse() {
DBG("processing HTTP request " << m_request.target() << " with payload of size " << m_request.payload_size().value_or(0))

	std::string target{m_request.target().data(), m_request.target().size()} ;

	// target: /tdaq/command?par1=3&par2=ab&v2=34
	// parse the parameters. TODO from Boost-1.82 there is Boost.URL parser
	size_t command_start = sizeof("/tdaq/")-1 ;
	size_t params_start = target.find('?', command_start) ;
	size_t command_end = target.find('/', command_start) ;
	auto command = target.substr(command_start, std::min(params_start,command_end) - command_start) ;
	std::map<std::string,std::string> params ;

	while( params_start != std::string::npos ) {
		size_t value_start = target.find('=', params_start + 1) ;
		if ( value_start == std::string::npos ) break ;
		auto key = target.substr(params_start+1, value_start - params_start - 1) ;
		params_start = target.find('&',value_start+1) ;
		auto value = target.substr(value_start+1, params_start - value_start - 1) ;
		params[key] = value ; 
	}

	try {
		auto[res, data] = m_processor->processSyncRequest(command, params, m_request.body()) ;
DBG("sync user command processed: " << res)
		if ( res == 0 ) {
			m_response.result(http::status::ok) ;	
		} else {
			m_response.result(http::status::bad_request) ; //  Standard HTTP user errors spans 400-451
			m_response.set("X-daq2soc-user-error", std::to_string(res)) ;
		}
		m_response.body() = data ;
		m_response.set(http::field::content_type, "application/octet-stream") ; // TODO allow user define? application/json if conversion to json was used
	} catch ( const std::exception& ex ) {
DBG("exception in sync user command: " << ex.what()) 
		m_response.set(http::field::content_type, "text/plain") ;
		m_response.result(http::status::internal_server_error) ; // 500
		m_response.set("X-daq2soc-user-exception", ex.what()) ; // pass original message in the header
		std::string msg("User exception in processing the request: " + std::string(ex.what())) ; // also something readable in the body
		m_response.body() = { msg.begin(), msg.end() } ;
	}
}

void
HTTPSessionTCP::writeResponse() {
	auto self = shared_from_this();

DBG("writing response")
	//m_response.content_length(m_response.body().size());
	m_response.prepare_payload() ;
	http::async_write(
		m_socket, m_response,
		[self](beast::error_code ec, std::size_t) {
DBG("HTTP response sent. Request keep alive: " << self->m_request.keep_alive())

			if ( !self->m_request.keep_alive() ) {
DBG("shutting down connection")	
				self->m_socket.shutdown(tcp::socket::shutdown_send, ec) ;
			} else {
				// read next request
DBG("continuing reading requests in the same HTTP session")	
				self->readRequest() ;
			}
		}) ;
}
}