#include <stdexcept>
#include <cstdlib>
#include <iostream>

#include "HTTPServer.hpp"

namespace { 
	size_t get_n_threads() {
    if ( auto p = getenv("DAQ2SOC_THREADPOOL_SIZE") ) { 
        int i = strtol(p, NULL, 10) ;
        if ( errno ) {
            { throw std::runtime_error("Failed to get number of threads from DAQ2SOC_THREADPOOL_SIZE environment") ; }
        return i ;
        }   
    }
    return tdaq::daq2soc::defaults::threadpool_size ;
}
}

namespace tdaq::daq2soc {

HTTPServer::HTTPServer(std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& address, short unsigned port, const std::string_view& domain):
m_threads(get_n_threads()),
m_ioc{m_threads},
m_acceptor{m_ioc, { net::ip::make_address(std::string(address)), port } },
//m_socket{net::make_strand(m_threadpool)},  // this is not available in boost-1.67 (Alma 8), so we need to construct threadpool below
m_socket{m_ioc},
m_processor{proc}, 
m_domain("/" + std::string(domain) + "/") {

}

void HTTPServer::start() {
	acceptStartSession(m_socket) ; // virtual

	m_threadpool.reserve(m_threads) ;
	for(auto i = m_threads; i > 0; --i) {
		m_threadpool.emplace_back( [this] { this->m_ioc.run(); });
	}

	m_processor->start() ; // start user thread
DBG("HTTP server started")
}

HTTPServer::~HTTPServer() {
	m_ioc.stop() ;
DBG("Stopping all worker threads")
	try {
		for( auto& t: m_threadpool ) { if ( t.joinable() ) t.join() ; }
	} catch ( std::exception & ex ) {
		std::cerr << ex.what() << std::endl ;
	}
DBG("HTTP server stopped")
}

namespace { void sig_func(int) {} ; }

void HTTPServer::wait() {
	sigset_t set ;
	sigemptyset(&set) ;
	sigaddset(&set, SIGINT) ;
	sigaddset(&set, SIGQUIT) ;
	sigaddset(&set, SIGTERM) ;
	signal(SIGINT, sig_func) ;
	signal(SIGQUIT, sig_func) ;
	signal(SIGTERM, sig_func) ;
	int sig ;
	sigwait(&set, &sig) ; // block and wait for a signal
}

void HTTPServer::acceptStartSession(tcp::socket& socket) {
	m_acceptor.async_accept(socket, [&](beast::error_code ec) {
DBG("Got HTTP connection, starting session")			
        if( !ec ) {
			std::make_shared<HTTPSessionTCP>(std::move(socket), m_processor, m_domain)->start() ; // handle the connection in separate http session
		}    	
		acceptStartSession(socket) ; // keep accepting
    }) ;
}

}
