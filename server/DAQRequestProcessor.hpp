#ifndef DAQ2SOC_DAQ_REQUEST_PROCESSOR_HPP
#define DAQ2SOC_DAQ_REQUEST_PROCESSOR_HPP

#include <tuple>
#include <vector>
#include <map>
#include <string>
#include <thread>
#include <atomic>

#include "AsyncDataProvider.hpp"

/**
 * @file DAQRequestProcessor.hpp
 * 
 * @brief Declares tdaq::daq2soc::DAQRequestProcessor class. A class implementing this interface should be provided by SoC application developer.
 */

namespace tdaq {
namespace daq2soc {


// in and out data types
/**
 * @typedef in_data_t
 * Type used for passing HTTP request payload to user function
 */
using in_data_t = std::vector<uint8_t> ;

/**
 * @typedef out_data_t
 * Type used for passing HTTP response (status + payload) from user function
 */
using out_data_t = std::tuple<int, std::vector<uint8_t>> ;

/**
 * @class DAQRequestProcessor
 * @brief This class meant to be extended by users to keep user data for the life cycle of the server daq2soc application.
 * 
 * Declares API to be implemented by end user: @ref processSyncRequest and @ref processAsyncRequest.
 * Inherits from @ref AsyncDataProvider. The user implementation can be compiled in a shared library loaded by daq2soc_server 
 * binary, or compiled in the standalone user application, using e.g. tdaq::daq2soc::HTTPServer to access daq2soc functionality.
 * 
 * @see AsyncDataProvider
 */
class DAQRequestProcessor: public AsyncDataProvider {


public:

DAQRequestProcessor() {} ;

virtual ~DAQRequestProcessor() {} ;

 
/** 
 * @brief Called by http server processing an http request: user code processes the command and returns status and data that are used to form a HTTP response
 * 
 * In case of 0 returned from the user call, HTTP response status set to 200.
 * Non-zero status return value results in 400 http response status, and the error code is passed to client in a header.
 * Any exception produced by this call will results in 500 response status and the exception text message as a http header.
 * 
 * @param endpoint a command received as part of URL 'http://server/<domain>/<endpoint>?param1=v1&...'
 * @param parameters a map of name=value parameters of the HTTP request
 * @param data_in payload passed in the request
 * 
 * @returns a tuple of int status code (0 in case of success) and a payload of type in_data_t to return
 * 
 */
virtual std::tuple<int, std::vector<uint8_t>>
processSyncRequest(
	const std::string& endpoint,
	const std::map<std::string, std::string>& parameters,
	const std::vector<uint8_t>& data_in) = 0 ;

} ;

} }

#endif