#ifndef DAQ2SOC_HTTP_SERVER_HPP
#define DAQ2SOC_HTTP_SERVER_HPP

/**
 * @file HTTPServer.hpp
 * 
 * @brief Declaration of HTTPServer class, that must be used by SoC developers to access DAQ2SoC server functionality.
 */

#include <string>
#include <chrono>
#include <thread>
#include <vector>
#include <memory>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio.hpp>

#include "DAQRequestProcessor.hpp"

using namespace std::string_literals ;

namespace beast = boost::beast ;         // from <boost/beast.hpp>
namespace http = beast::http ;           // from <boost/beast/http.hpp>
namespace net = boost::asio ;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp ;       // from <boost/asio/ip/tcp.hpp>

/**
 * @typedef body_t
 * @brief data structure for a http request/response body
 * 
 */
using body_t = http::vector_body<uint8_t> ;

/**
 * @typedef data_t
 * @brief Data is a structure (\c std::tuple) containing an integer return code from a command and a Payload
 * @see tdaq::daq2soc::payload_t
 */
using data_t = std::vector<uint8_t> ;

namespace tdaq {
namespace daq2soc {

namespace defaults {

constexpr size_t threadpool_size{8} ; // number of threads started by a server
constexpr std::string_view address_ip4{"0.0.0.0"} ; // ipv4 interface to buid to 
constexpr std::string_view address_ip6{"0::0"} ; // ipv6 interface to buid to 
constexpr std::string_view domain{"tdaq"} ; // domain, part of URL http://host:<port>/<domain>/<endpoint>
constexpr short unsigned port = 8080 ; // plain TCP port to listen on
constexpr short unsigned port_ssl = 8443 ; // SSL socket port to listen on

}

class HTTPServer ;
class HTTPServerSSL ;

/// Internal class, not exposed to daq2soc user API.
/**
 *  instances of this class are created by HTTPServer to handle each incoming connection
 */
class HTTPSession {
friend class HTTPServer ;
friend class HTTPServerSSL ;

protected:
    // The buffer for performing reads.
    beast::flat_buffer m_buffer{8192} ;

    // The request message.
    http::request<body_t> m_request ;

    // The response message.
    http::response<body_t> m_response ;

    std::shared_ptr<DAQRequestProcessor> m_processor ; // handle to the user code, processing the requests
    std::string_view m_domain ; // "tdaq"

    HTTPSession(std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& domain) ;
    ~HTTPSession() ;

    // Initiate the asynchronous operations associated with the connection.
    void start() ;

    HTTPSession(HTTPSession&&) = delete ;
    HTTPSession& operator=(HTTPSession&&) = delete ;
    HTTPSession(const HTTPSession&) = delete ;
    HTTPSession& operator=(const HTTPSession&) = delete ;

// common methods
	void processRequest() ;
    bool processAsyncRequest() ;
	void createResponse() ;

// tp be reimplemented in derived classes
    virtual void writeResponse() = 0 ;
    virtual void readRequest() = 0 ;
} ;

/// Internal class, not exposed to daq2soc user API.
/**
 * Implementation of HTTPSession for plain TCP stream.
 */
class HTTPSessionTCP: public HTTPSession, public std::enable_shared_from_this<HTTPSessionTCP> {
    
public:
    HTTPSessionTCP(tcp::socket socket, std::shared_ptr<DAQRequestProcessor> proc, const std::string_view& domain) ;
    ~HTTPSessionTCP() ;

protected:
    // The socket for the currently connected client.
    tcp::socket m_socket ;

    // reimplemented 
    virtual void writeResponse() override ;
    virtual void readRequest() override ;
} ;

/**
 * @brief Class that can be instantiated in a SoC user application to access DAQ2SoC server functionality.
 * 
 * Caller should provide an instance of a class implementing @ref tdaq::daq2soc::DAQRequestProcessor API (e.g. @ref tdaq::daq2soc::UserExample).
 * After constructing the server instance call @ref start() to start the request processing cycle.
 * Then you can call @ref wait() in case you need to block the main thread of your application.
 * If you want to stop processing, just destroy the instance of HTTPServer.
 * 
 * @code
    try {
    	tdaq::daq2soc::HTTPServer server(std::make_shared<tdaq::daq2soc::UserExample>(), "0.0.0.0", 8080) ;
        server.start() ;
	    server.wait() ; // blocks the main thread, waits for a process termination signal - no need to call if you don't need to block.
    } catch (std::exception const& e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
 * @endcode
 *
 * @see tdaq::daq2soc::HTTPServerSSL for SSL-based variant of this class
 */
class HTTPServer {

protected:

    // boost::asio::thread_pool m_threadpool ; // no constructor of context from threadpool
    size_t m_threads ; // N of threads in the pool
    std::vector<std::thread> m_threadpool ;
    net::io_context m_ioc ;
	tcp::acceptor 	m_acceptor ;
    tcp::socket 	m_socket ;
    std::shared_ptr<DAQRequestProcessor> m_processor ; // handle to the user code, processing the requests
    std::string     m_domain ;

	// accept connection and start session
	virtual void acceptStartSession(tcp::socket& socket) ;
    
    HTTPServer(HTTPServer&&) = delete ;
    HTTPServer& operator=(HTTPServer&&) = delete ;
    HTTPServer(const HTTPServer&) = delete ;
    HTTPServer& operator=(const HTTPServer&) = delete ;

public:


    /**
     * @brief Constructor. To start accepting connections, call @ref #start()
     * 
     * Example of use:
     * 
     @code

// create server listening on ipv4, default port number (8080) and default domain ("tdaq")
tdaq::daq2soc::HTTPServer server(std::make_shared<tdaq::daq2soc::UserExample>()) ;
server.start() ;
// create server listening on ipv4, specific port number (8088) and domain ("trigger")
tdaq::daq2soc::HTTPServer server(std::make_shared<tdaq::daq2soc::UserExample>(), defaults::address_ip4, 8088, "trigger") ;
server.start() ;

     @endcode

     * @param daq_proc shared pointer ti a user implementation of the DAQRequestProcessor API
     * @param address interface to bind to (default is all interfaces on IPv4: "0.0.0.0", use \c defaults::address_ip6 for listening on IPv6).
     * @param port TCP port to listen on (default: 8080)
     * @param domain part of the request URL \c %http://host:<port>/<domain>/<endpoint> (default: "tdaq")
     *
     */
	HTTPServer( std::shared_ptr<DAQRequestProcessor> daq_proc,
                const std::string_view& address = defaults::address_ip4,
                short unsigned port = defaults::port,
                const std::string_view& domain = defaults::domain) ;

	virtual ~HTTPServer() ;

    /// starts accepting connections, call it right after construction
    void start() ;

    /// blocks waiting for program termination signal
	void wait() ;

} ;

}}

#endif
