#include <iostream>

#include "AsyncDataProvider.hpp"

namespace tdaq {
namespace daq2soc {

AsyncDataProvider::AsyncDataProvider():
m_stop_p{},
m_stop_f{m_stop_p.get_future()},
m_data_ready_p{},
m_data_ready_f{m_data_ready_p.get_future().share()}
{
DBG("AsyncDataProvider constructed");
}

AsyncDataProvider::~AsyncDataProvider() {
	m_stop_p.set_value(true) ;
	if ( m_thread.joinable() ) { m_thread.join() ; }
DBG("AsyncDataProvider destructed");
}

void AsyncDataProvider::start() {
DBG("launching thread")
	m_thread = std::thread{&tdaq::daq2soc::AsyncDataProvider::processAsyncRequest, this} ;
}

bool AsyncDataProvider::block(std::chrono::milliseconds timeout) {
	auto status = m_stop_f.wait_for(timeout) ;
	if ( status == std::future_status::ready ) {
DBG("stopping the thread") ; 
		return true ;
	} ;
	return false ;
}
 
// called from asynchronous user thread wishing to publish some data
// fullfills a promise, serving as a FIFO of size 1.
// On the reciveing part of this FIFO, all long polling calls (blocking on a shared future) get notified and return data to requestors.
bool AsyncDataProvider::fulfillAsyncRequest(const std::string& id, std::unique_ptr<data_t> ready_data) {

std::lock_guard<std::mutex> lock(m_mtx) ;
	try {

// DBG("Thread " << std::this_thread::get_id() << ": data ready for " << id)
		m_data_ready_p.set_value({id, std::move(ready_data)}) ; // notify long requests waiting on this future (see waitForAsyncData)
	} catch ( const std::future_error& ex ) {
		if ( ex.code() == std::future_errc::promise_already_satisfied ) { // this means no one is waiting for the data, so we need to reset the future/promise
// DBG("promise_already_satisfied, but we got fresh data")
			m_data_ready_p = std::promise<promised_data>() ; // reset
			m_data_ready_f = m_data_ready_p.get_future().share() ;
			return true ;
		}
DBG("future_error")
		return false ; // failure, exception?
	}
	return true ;
} 

// blocks on m_data_ready_f
// called from long polling http request handler
// empty values returned on timout, ready data {ID,data_t} returned if data is available
std::tuple<std::string, std::shared_ptr<data_t>>
AsyncDataProvider::waitForAsyncData(std::chrono::seconds timeout)
{
	auto data_ready{m_data_ready_f} ; // get a copy per thread/call
DBG("Thread " << std::this_thread::get_id() << ": waiting for user data")
	if ( data_ready.wait_for(timeout) == std::future_status::ready ) {
DBG("Thread " << std::this_thread::get_id() << ": new data published")
		auto[ready_id,data] = data_ready.get() ;  // will fail if called from two requests: to fix use shared_future and copy the data
DBG("Thread " << std::this_thread::get_id() << ": new data retreived for id " << ready_id) ;	
		std::lock_guard<std::mutex> lock(m_mtx) ;
		m_data_ready_p = std::promise<promised_data>() ; // reset
		m_data_ready_f = m_data_ready_p.get_future().share() ;

		return {ready_id, data} ;
	} ;
// DBG("Thread " << std::this_thread::get_id() << ": no data")
	return {"", std::make_unique<data_t>()} ;
}

	// return a copy (move?) of the ready data
	// it is assumed that this is called from the polling request after waitForAsyncRequest returned the ID
/*
	std::unique_ptr<data_t> retrieveData(const std::string& id) {
		// TODO reset m_data_ready_p/f
		// TODO remove this id from collections
		if ( auto it = ) 
			std::unique_ptr<data_t> r = std::move(m_async_data[id]) ;

			return r ;
		} catch ( const std::future_error& ex ) {
			
			return std::unique_ptr<data_t>() ;
		}
	}
*/

}}