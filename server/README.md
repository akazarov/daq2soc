### Implementation of the server DAQ2SoC functionality

This part of the daq2soc repository contains the C++ implementation of the server functionality: the source code, the pre-built binaries for running a standalone server, and user examples. The implementation is done in C++ using Boost/Beast libraries for HTTP/TCP(SSL) layer.

#### Introduction to API

The API provides the server counterpart of the daq2soc client functionality: server-side user code uses the API to process synchronous and asynchronous requests sent from the client using `tdaq::daq2soc::Sender` API. There are two classes involved: `tdaq::daq2soc::DAQRequestProcessor` and `tdaq::daq2soc::AsyncDataProvider` (see the Doxygen section below), declaring pure virtual methods that need to be implemented in a user code by instantiating a class inheriting from `DAQRequestProcessor`:

- `tdaq::daq2soc::DAQRequestProcessor::processSyncRequest()`: user code for processing a remote request formed by `tdaq::daq2soc::Sender::sendCommandSync()`.

- `tdaq::daq2soc::AsyncDataProvider::processAsyncRequest()`: despite the name (given for the sake of symmetry to the processSyncRequest), there is no request that is processed here: this function with user code is executed in a separate thread asynchronously to other requests from Sender. Instead, the user code can publish some user data with an ID by calling `fulfillAsyncRequest` at any moment.

- `tdaq::daq2soc::AsyncDataProvider::fulfillAsyncRequest()`: you should call this function from `processAsyncRequest` (or from any other thread in your application) to publish some user data passed from your code as std::unique_ptr<data_t>. All the clients (Sender instances), connected to this server are notified about new data in corresponding callbacks in the `tdaq::daq2soc::Sender` API.

#### Using the library

After you implemented the required API in a user class (e.g. `UserExample`) and compiled it, you have two ways of creating the runtime setup:

- run pre-built daq2soc server binary (`daq2soc_server` or `daq2soc_server_ssl`) and load the user code as a shared library (similarly to what was done in the prototype with nginx server/module architecture)

- build your own application and loading `daq2soc` code from a pre-built server library (`libdaq2soc_server.so` or `libdaq2soc_server_ssl.so`)

See [declaration](examples/UserExample.hpp) and [implementation](examples/UserExample.cpp) of a `tdaq::daq2soc::UserExample` class providing a working example of the server-side user functionality. The corresponding Makefile target is `user_lib`.

The following piece of code must be added to the source code of user library, it is used by the server to instantiate user class from the loaded dynamic library:

```c
extern "C" {
DAQRequestProcessor* create() {
    return new UserExample() ;
}}
```

To use daq2soc server functionality in a user standalone application, instantiate `tdaq::daq2soc::HTTPServer` class, as shown in the [example](examples/standalone_example_main.cpp) and also below. The application must link with `libdaq2soc_server.so` library, see `user_app` Makefile target.

```cxx
tdaq::daq2soc::HTTPServer server(std::make_shared<tdaq::daq2soc::UserExample>(), "0.0.0.0", 8080) ; // you can skip last two arguments and use default interface binding and port
server.start() ; // start accepting connections
server.wait() ; // in case you need to block the current thread and wait for termination signals
```

##### Compiling sources and examples

Makefile defines the following targets:

- *server*: a standalone server that dynamically loads a user library (compiled separately e.g. as user_lib target) and processes daq2soc requests
- *library*: a daq2soc_server.so library that can be loaded from a separate user application (compiled e.g. as *user_app* target)
- *server_ssl* *library_ssl*: SSL versions
- *user_lib*: example of a library containing implementation of user API, the tdaq::daq2soc::UserExample class.
- *user_app*: example of an application that is linked with `daq2soc_server.so` and uses the provided API.

To compile the server code (server daq2soc library and a standalone server binary), you need to install `boost-1.75` or newer development package (e.g. `dnf install boost-devel` on a red-hat distribution). For SSL versions of the binaries, `openssl-devel` package needs to be added.

```bash
make -j5 server library
make -j5 ssl
```

These two binaries are also available as pre-built in [here](binaries/aarch64) and can be used on any `aarch64` Linux distribution with GLIBC version higher then 2.27 (e.g. RHEL8 and RHEL9 families).  
You can also compile only the user library (to be used with the pre-built server), no dependencies are needed at this case.

```bash
make -j5 user_lib
```

or compile a user application that is linked with `daq2soc_server.so` (and also with UserExample.o module):

```bash
make -j5 user_app
```

All the binares are produced in `build-aarch64` folder.

##### Running the server and tests

Having compiled a user library, you can run `daq2soc_server` and load your code in the following way:

```bash
LD_LIBRARY_PATH=`pwd`/build-aarch64 binaries/aarch64/daq2soc_server 0.0.0.0 8080 tdaq libUserExample.so
```

If you compiled a standalone user applicaiton, run it similarly:

```bash
LD_LIBRARY_PATH=`pwd`/binaries/aarch64 ./build-aarch64/server_ex 0.0.0.0 8080
```

To test the server, you can make locally requests with help of curl utility. The provided example of DAQRequestProcessor class implements processing of few example requests:

Sending it a `http POST` request with some payload and a command (to sleep for 2 seconds):

```bash
curl -i -X POST --output - -d '{"key1":"value1", "key2":"value2"}' 'localhost:8080/tdaq/sleep?value=2&test=3'
```

Another implemented example gets the load of the server from `/proc/loadavg` file:

```bash
curl -i -X POST --output - 'localhost:8080/tdaq/get_load'
```

There is a `probe` function implemented in the module, which can be user to probe the server/module is healthy using the `http HEAD` request:

```bash
curl -i -I 'localhost:8080/tdaq/probe'
```

- one should expect `HTTP/1.1 200 OK` response.

Get a bad (non-0) response from user code:

```bash
curl -i -X POST --output - 'localhost:8080/tdaq/bad_request'
```

or an exception raised in user code:

```bash
curl -i -X POST --output - 'localhost:8080/tdaq/get_exception'
```

#### Package content

*DAQRequestProcessor.hpp* declaration of the API that needs to be implemented in the user code  
*HTTPServer.\** *HTTPSession.\* AsyncDataProvider.\** - library sources  
*server_main.cpp* - main function of the daq2soc_server application  
*examples* - examples of user library and binary  
*binaries/aarch64* - pre-built server application and library

#### Doxygen

[DAQRequestProcessor](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/classtdaq_1_1daq2soc_1_1_d_a_q_request_processor.html)
[HTTPServer](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/classtdaq_1_1daq2soc_1_1_h_t_t_p_server.html)
