#ifndef TDAQ_DAQ2SOC_DAQSOC_HPP
#define TDAQ_DAQ2SOC_DAQSOC_HPP

#include <tuple>
#include <vector>
#include <map>
#include <list>
#include <string>
#include <string_view>
#include <future>
#include <memory>
#include <chrono>
#include <thread>
#include <mutex>
#include <functional>
#include <optional>

#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/core/flat_buffer.hpp>

#include "Exceptions.hpp"
#include "Stream.hpp"
#include "data2json.hpp"

/**
 * @file daqsoc.hpp 
 * 
 * @brief Defines the client part of daq2soc API, mainly implemented in @ref tdaq::daq2soc::Sender class.
 * 
 * @see tdaq::daq2soc::Sender
 */
namespace tdaq {
namespace daq2soc {

/**
 * @typedef callback_t
 * @brief User callback for asynchronous data publication
 * 
 * As parameters of the function user gets data ID, pointer to data, and optionally an exception - typically indicating that there were communication issues.
*/
using callback_t = std::function<void(const std::string&, std::shared_ptr<data_t>, std::exception_ptr)> ;

namespace http = boost::beast::http ;
using tcp = boost::asio::ip::tcp ; 
using Response = http::response<http::vector_body<payload_byte_t>> ;
using Request = http::request<http::vector_body<payload_byte_t>> ;
using namespace std::chrono_literals ;

const int http_version {11} ;

/**
 * @var default_comm_timeout 
 * default communication timeout applied to underlying TCP socket operations
 */
constexpr std::chrono::seconds default_comm_timeout = 5s ;

template <class TStream = TCPStream> class Sender ; // forward declaration

/** @class tdaq::daq2soc::AsyncHandler
 * 
 *  @brief A handler for asynchronously expected result of @ref Sender#sendRequestAsync call.
 * 
 *  Template class, two instantiations are provided: AsyncHandlerTCP (also available as simply AsyncHandler) and AsyncHandlerSSL.
 *  An smart pointer to instance of this class is returned from @ref Sender#sendRequestAsync call.
 *  It is used to get the result returned by the command handling on the remote server. 
 *
 *  @tparam TStream a (base) class used as an underlying transport stream, see @ref TCPStream and @ref SSLStream for possible implementations. Default is TCPStream.
 * 
 *  @note: This class holds and opens a dedicated socket connection to server to handle this single request, waiting for the server to process it.
 *  Must be used with care in case many commands is sent in parallel: the server performance may be limited and in case many long-processing 
 *  requests are foreseen (or from many clients), increase the number of server threads, or (better) consider using @ref Sender#getDataAsync -
 *  a truly asynchronous call which does not block in waiting for the server response and does not create additional connection per call.
 */
template <class TStream = TCPStream>
class AsyncHandler: public TStream {
    
    // only two classes have rights to instantiate it
    friend class Sender<TStream> ;

    using TStream::m_req, TStream::m_res, TStream::m_buffer, TStream::m_stream ; // access to protected members of base

    std::promise<data_t>        m_promised ;
    std::future<data_t>         m_future ;

    /// private, only constructed as shared_ptr by Sender class
    AsyncHandler(const std::string_view& host_port) ;

    // posts a HTTP read/write to async threadpool
    void req_async(std::chrono::seconds timeout) ;

    template <typename T>
    void req_async(const std::string_view& endpoint, const std::map<std::string, std::string>& parameters, T&& data_in, std::chrono::seconds timeout) {
        TStream::m_req = {   endpoint == "probe" ? http::verb::head : http::verb::post,
                    TStream::prepare_parameters(endpoint, parameters),
                    http_version,
                    std::forward<T>(data_in) } ; // preserve rvalue reference and avoid copying

        req_async(timeout) ;
    }

public:

    /**
     * @brief Gets the Data object as a result of asynchronous call to @ref Sender#sendRequestAsync, a blocking call
     * 
     * A shared pointer to it is returned in Sender::sendRequestAsync call. Blocks until it is retrieved from the daq2soc server,
     * or until timeout occurs - which causes TimeoutException to be thrown.
     * 
     * @throw TimeoutException
     * 
     * @return data_t user data 
     * 
     * @see Sender::sendRequestAsync
     */
    data_t getData() ; // blocks until timeout occurs - which causes TimeoutException to be thrown

     /**
     * @brief Gets the \c std::optional<data_t> object allowing to block and wait for data for specified amount of time.
     * 
     * @param timeout to wait for the result. If this is set to a bigger value then the timeout in the sendRequestAsync call,
     * TimeoutException will be thrown.
     * 
     * @throw TimeoutException
     * @return data_t \c std::optional<data_t> if the remote request was completed within timeout, otherwise an empty \c optional<>
     * 
     * @see Sender::sendRequestAsync
     */
    template< class Rep, class Period >
    std::optional<data_t>
    getData(std::chrono::duration<Rep, Period> timeout) {
        if ( auto r = m_future.wait_for(timeout) ; r == std::future_status::ready ) {
            return m_future.get() ;
        }
        return std::optional<data_t>() ;
    }

    ~AsyncHandler() ; // destructed from shared_ptr in user space

} ;

/**
 * @typedef AsyncHandlerTCP
 * @brief a shortcut for AsyncHandler over TCPStream. One can also use simply AsyncHandler. There is also @ref AsyncHandlerSSL.
 */
using AsyncHandlerTCP = AsyncHandler<TCPStream> ;

/**
 * @typedef AsyncHandlerSSL
 * @brief a shortcut for AsyncHandler over SSLStream.
 */
using AsyncHandlerSSL = AsyncHandler<SSLStream> ;

/// Internal class, not exposed to daq2soc user API.
template <class TStream = TCPStream>
class LongPollHandler: public TStream {
    friend class Sender<TStream> ;

    std::promise<data_t>        m_promised ;
    std::shared_future<data_t>  m_future ;

    Sender<TStream>&            m_sender ; // parent, used to publish data received in long polling

    std::mutex                  m_mtx ;
    bool                        m_running ;

    /// private constructor, accessible from Sender
    LongPollHandler(const std::string_view& host_port, Sender<TStream>& sender) ;
    
    // runs looping long poll requests (if not yet running for this Sender)
    void run () ;
    void stop() ;

public:
    ~LongPollHandler() ; // destructed from shared_ptr upon program termination

} ;

/** @class tdaq::daq2soc::Sender
 * 
 *  @brief Class implementing communication with a particular SoC host running a daq2soc server application.
 *
 *It provides different types of request sending possibilities:
 * 
 *- blocking synchronous: blocks waiting for a response from the server which processes the request by calling user code.
 * This is done in a single TCP/HTTP connection, and all calls done by blocking @ref #sendRequestSync() are sequentially processed on the server.
 * The synchronous method is preferred when one needs to send many commands that require minimal processing time on the server.
 * 
 *- pseudo-asynchronous (non-blocking synchronous): @ref #sendRequestAsync() opens a dedicated connection to the server to handle this single request. The call
 *returns immediately a @ref AsyncHandler that can be used to get the result of request in blocking manner.
 *This method can be used for running in parallel few request requiring some (relatively) long processing time, however it must be used with care in case many commands are sent in
 *parallel: there may be limitations on number of open files/sockets (e.g. order of 1000), and also on the number of processing threads on the server.
 *
 *- truly asynchronous: a subscription-like communication pattern is implemented with @ref #getDataAsync() methods.
 * 
 *Both sendRequestSync and sendRequestAsync remotely call server API @ref tdaq::daq2soc::DAQRequestProcessor#processSyncRequest().
 * 
 *Templated class, while instantiating one need to provide either TCPStream (default) or SSLStream class. daq2soc library contains implementation for both.
 *Alias types are provided: @ref SenderTCP (also simply @ref Sender) and @ref SenderSSL.
 * 
 * @tparam TStream an underlying transport stream, either @ref tdaq::daq2soc::TCPStream or @ref tdaq::daq2soc::SSLStream. Default is TCPStream.
 */
template <class TStream>
class Sender: public TStream {
friend class LongPollHandler<TStream> ;

private:
    
    std::shared_ptr<LongPollHandler<TStream>> m_lph ;

    /** container for data promised in async communication, key: an id of the data, returned from server */           
    std::map<std::string, std::list<std::promise<data_t>>>  m_async_promised_data ; 
    std::mutex m_mutex ;
 
    boost::asio::thread_pool    m_cb_threadpool ; // dedicated TP to handle user callbacks without waiting for them
    std::vector<callback_t>     m_user_callbacks ;

    // called by LongPollHandler upon receiveing some async data
    void fulfill_data(const std::string& id, data_sp) ;
    void fulfill_data(const std::exception_ptr) ;

    Sender(const Sender&) = delete ;
    Sender(Sender&&) = delete ;
    Sender& operator=(const Sender&) = delete ;
    Sender& operator=(Sender&&) = delete ;

public:

    /**
     * @brief Constructor. Connects to a particular daq2soc server application listening on specified port.
     * 
     * @param host_port A host:port TCP address to connect to. Subsequent commands will be sent to the (HTTP) daq2soc server running on this host (on a pre-defined port number).
     * A single tcp connection to this address is established and is used to send synchronous commands to the server. Parameters shall have form \c "host:port", if \c "port" is omitted,
     * the default value \c 8080 is used.
     * @throws SenderException
     * 
     * @see #Sender(const std::string_view&, const std::string_view&)
     */
    explicit Sender(const std::string_view& host_port) ;

    /**
     * @brief Constructor allowing to define additional device name
     * 
     * Equivalent to @ref #Sender(const std::string_view&) but adds one more parameter "device" which is used to build request URL with extra element in the path like
     * \c %http://proxy_server:port/<device>/tdaq/<endpoint>?param1=...
     * This can to be used to address multiple SoC servers via a HTTP proxy server, e.g. for apache the proxy configuration may look like
     * @code
    <LocationMatch "^/sm(\d)+/tdaq/">
        ProxyPass  http://192.168.0.${d}:8080/tdaq/
    </Location>
       @endcode
       that would allow to build URLs like \c %http://proxy_server/sm218/tdaq/rc?param=value , \c %http://proxy_server/sm226/tdaq/get_state etc.
     */
    Sender(const std::string_view& host_port, const std::string_view& device) ;

    ~Sender() ;

    /**
     * @brief Synchronously sends a command to server using HTTP and blocks waiting for a result.
     *
     * Command name (string_view) is translated into a http endpoint as part of 
     * \c %http://server:port/<device>/tdaq/<endpoint>?param1=value1&param2=value2 request URL.
     * Example commands implemented in test server in this package: get_counter, get_histogram.
     *
     * @param endpoint a simple string command sent to a daq2soc server, translated to an endpoint in HTTP request URL
     * @param parameters a map of {'name','value'} pairs of named command parameters and values
     * @param data_in a vector of bytes, a user-supplied data. Uses "forward reference" type. To avoid copying, you can std::move your data to it.
     * @param timeout communication timeout on getting the http response from the server (seconds). Default is 5 seconds.
     * 
     * Example of use:
     * @code
     * 
    std::vector<tdaq::daq2soc::payload_byte_t> vector_1M(1024*1024, 0xFFFF) ;
    auto res = mysender.sendRequestSync("bad_request", parameters, std::move(vector_1M)) ; // move and do not use it again
     * @endcode
     * @throws TimeoutException in case timeout is reached
     * @throws SenderException in case of other errors, like connection closure etc.
     * 
     * @return data_t result of request processing
     * @see payload_t
     */
    template <typename T>
    data_t
    sendRequestSync (   const std::string_view& endpoint, 
                        const std::map<std::string, std::string>& parameters,
                        T&& data_in,
                        std::chrono::seconds timeout = default_comm_timeout) {
        // connect() ; // moved to base
                    
        TStream::m_req = {  endpoint == "probe" ? http::verb::head : http::verb::post,
                            TStream::prepare_parameters(endpoint, parameters),
                            http_version,
                            std::forward<T>(data_in) } ; // preserve rvalue reference and avoid copying
        
        return TStream::sendRequestSync(timeout) ;
    }

    /**
     * @brief Synchronously sends a request to server. Allows to send tuple-based user data, internally converted to Json.
     *
     * Command name is translated into a http endpoint as part of 
     * \c %http://server:port/<device>/tdaq/<endpoint>?param1=value1&param2=value2 request URL.
     * Example endpoints implemented by the test server in this package: get_counter, get_histogram, get_exception.
     *
     * @param endpoint a simple string command sent to a daq2soc server, translated to an endpoint in HTTP request URL
     * @param parameters a map of \c {'name','value'} pairs of named command parameters and values
     * @param user_data a tuple of user objects to pass to the server in the request body, using data2json.
     * @param timeout communication timeout on getting the http response from the server (seconds). Default is 5 seconds.
     *
     * Example of use:
     * @code
     *
// sending part
    std::tuple my_structure { "a message", 10, std::array{0,1,2,3,4,5} } ;
    auto res = mysender.sendRequestSync("bad_request", parameters, my_structure) ;

// receiving part, DAQRequestProcessor::processSyncRequest(... data_in ... )    
    std::string_view json { (const char*)data_in.data(), data_in.size() } ; 
    auto data_in_tuple = tdaq::daq2soc::json2data<std::string, int, std::array<int,6>>(view) ; // provide the template parameters identical to the sending structure
    std::cerr << "got message: " << std::get<0>(data_in_tuple) ;

     * @endcode
     *  
     * @throws TimeoutException in case timeout is reached
     * @throws SenderException in case of other errors, like connection closure etc.
     * 
     * @return data_t result of request processing
     */   
    template <typename... Ts>
    data_t
    sendRequestSync (   const std::string_view& endpoint, 
                        const std::map<std::string, std::string>& parameters,
                        std::tuple<Ts...>& user_data,
                        std::chrono::seconds timeout = default_comm_timeout) {
        // payload_t pl = data2json_v(tup) ; // craft json in vector<char> and move it to HTTP layer, avoiding any extra copy
        auto js = data2json(user_data) ;
        payload_t pl {std::begin(js), std::end(js)} ;
        TStream::m_req.set(http::field::content_type, "application/daq2soc-json; charset=utf-8") ; // hint server to decode the body to c++, but it still need to know         
        return sendRequestSync(endpoint, parameters, std::move(pl), timeout) ;
    }


    /**
     * @brief Asynchronously sends a request to server, returning immediately a handle to asynchronous result.
     *
     * A dedicated socket connection to server is opened to handle every single asynchronous request, multiple requests can be handled in parallel.
     * A thread pool on client and server side are used to handle the requests. Default size should be increased in case many requests requiring long processing are foreseen.
     *
     * The endpoint name (string) forms a part of the request URL \c %http://server:port/tdaq/<endpoint>?param1=value1&param2=value2
     * Example requests implemented in an example server in this package: get_counter, get_histogram, get_load and more (see UserExample#processSyncRequest).
     * All exceptions (@ref TimeoutException, @ref SenderException) are thrown in AsyncHandler#getData().
     * @param endpoint a simple string command sent to a daq2soc server
     *
     * @param parameters a map of 'name'='value' pairs of named command parameters and values.
     * @param data_in a vector of bytes, user-supplied data. You can either copy or move from it.
     * @param timeout communication timeout on getting the http response from the server (seconds). Default is 5 seconds.
     * 
     * @return a shared pointer to AsyncHandler instance. It needs to be kept in user space until the result is received and retrieved from the AsyncHandler.
     * @see payload_t 
     * @see AsyncHandler
     */
    template <typename T>
    std::shared_ptr<AsyncHandler<TStream>> 
    sendRequestAsync (  const std::string_view& endpoint,
                        const std::map<std::string, std::string>& parameters,
                        T&& data_in,
                        std::chrono::seconds timeout = default_comm_timeout) {

            std::shared_ptr<AsyncHandler<TStream>> handler = std::shared_ptr<AsyncHandler<TStream>>(new AsyncHandler<TStream>(TStream::m_host_port)) ;
DBG("created new AsyncHandler") ;
            handler->req_async(endpoint, parameters, std::forward<T>(data_in), timeout) ; 
            return handler ;
    } 

    /// Gets data that is published asynchronously on the server by @ref AsyncDataProvider#fulfillAsyncRequest
    /**
     * Implements asynchronous communication to server using long polling technique.
     * In background, client starts a permanent dedicated connection to the server, that is eventually delivers a data object when server side user code
     * fullfills a promise by calling @ref AsyncDataProvider#fulfillAsyncRequest(). The data is returned to client and the future object is notified.
     * In case of non-recoverable error (e.g. disconnected server), exception is propagated to the \c future<data_t>.
     * 
     * @param id a string ID of the expected data (free format, the same that is used on server to publish it)
     * @return a future of the data. Once it is published and retrieved, you can request it again (see examples/test_async.cpp).
     * To continuously receive all data from server in a callback, use @ref Sender#getDataAsync.
     * 
     * @see Sender#getDataAsync(callback_t callback)
    */
    std::future<data_t>
    getDataAsync ( const std::string& id ) ;                    
 
   /// Gets all data that is published asynchronously on the server by @ref AsyncDataProvider#fulfillAsyncRequest
   /**
     * Get data asynchronously posted by the daq2soc server in a callback. Data for all IDs are retrieved, user should select the needed ones.
     * If this is not desirable, subscribe to particular ID in Sender#getDataAsync(const std::string& id) method. 
     * 
     * @param callback a function with @ref callback_t signature
    */
    void
    getDataAsync ( callback_t callback ) ;
} ;

/**
 * @typedef SenderSSL a shortcut for Sender over SSLStream.
 */
using SenderSSL = Sender<SSLStream> ;


/**
 * @typedef SenderTCP a shortcut for Sender over TCPStream.
 */
using SenderTCP = Sender<TCPStream> ;


}} // tdaq::daq2soc

inline std::ostream& operator << ( std::ostream& str, const tdaq::daq2soc::payload_t& data) {
    for ( size_t i = 0 ; i < data.size();  i++ ) {
        str << data[i] ;
    }
    return str ;
}

#endif