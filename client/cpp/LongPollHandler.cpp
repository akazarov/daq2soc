#include <future>

#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/beast/version.hpp>

#include "Stream.hpp" // SSLStream
#include "daqsoc.hpp" // Sender

namespace http = boost::beast::http ;

namespace tdaq {
namespace daq2soc {

template <class TStream>
LongPollHandler<TStream>::LongPollHandler(const std::string_view& host_port, Sender<TStream>& sender)
:TStream(host_port), m_sender(sender), m_running{false}
{}

template <class TStream>
void LongPollHandler<TStream>::run() {

	std::lock_guard lock(m_mtx) ;

	if ( m_running ) {
		return ;
	}

	TStream::connect() ;

DBG("Long poll connected to " << m_host) 

	m_running = true ;

	boost::asio::socket_base::keep_alive option(true) ;
	TStream::socket().set_option(option) ;

	// asyncronously execute a polling code that writes a async request to 
    boost::asio::spawn(TStream::stream().get_executor(), [this](boost::asio::yield_context yield) {

		bool reconnect {false} ;

		this->m_req = { http::verb::subscribe, TStream::prepare_parameters("",{}), http_version } ; // http::verb::subscribe indicates that this is long polling request. TODO pass the poll time in a header.
		this->m_req.set(http::field::host, this->m_host) ;
		this->m_req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING) ;
		this->m_req.set("X-daq2soc-async-poll", "true") ;
		this->m_req.prepare_payload() ;
		this->m_req.keep_alive(true) ;
		this->m_running=true ;

		while ( true ) {
			try {
				if ( reconnect ) {
					this->reconnect() ; // throw SenderException
					reconnect = false ;
					boost::asio::socket_base::keep_alive option(true) ;
					TStream::socket().set_option(option) ;
				}

				Response res ;
				boost::beast::flat_buffer buffer ; 

				[[maybe_unused]] auto sent = http::async_write(this->stream(), this->m_req, yield) ;
DBG("Sent " << sent << " bytes") ;
				[[maybe_unused]] auto received = http::async_read(this->stream(), buffer, res, yield) ;
DBG("Received " << received << " bytes") ; 
				// empty response means no data yet available, so just repeat asking

DBG("Async request returned with http result " << res.result_int()) ;

				if ( res.result() == http::status::no_content ) {
DBG("Async request returned nothing, continue polling") ;
				} else 
				if ( res.result() == http::status::ok ) {
					try {
DBG("Async request returned some data, fulfilling the promise for " << res.at("X-daq2soc-async-id")) ;
						if ( !res.at("X-daq2soc-async-id").empty() ) {
							std::string id{res.at("X-daq2soc-async-id")} ; // extract data id from http header
							//payload_t p{std::move(res.body())} ; 
DBG("Notifying user with data for ID " << id) ;
							auto sp = std::make_shared<data_t>(0, std::move(res.body())) ;
							m_sender.fulfill_data(id, sp) ; // calls fulfill_data(const string&, payload_t&&)
						}
					} catch ( const std::out_of_range& ex ) {
DBG("Unexpected, X-daq2soc-async-id is not found in log polling response") ; // TODO fulfill_data(std::current_exception());
					}
				}
			} catch ( boost::system::system_error const& ex ) {
				if ( 	ex.code() == http::error::end_of_stream || // next can be "Connection reset by peer"
						ex.code() == boost::asio::error::connection_reset || 
						ex.code() == boost::asio::error::connection_refused ) { 
DBG("LongPollHandler received connection exception, will try to reconnect" << ex.what() ) ; 
					reconnect = true ;
					std::this_thread::sleep_for(std::chrono::seconds(1)) ;
					continue ; // attempt to reconnect
				} ;
				if ( ex.code() == boost::asio::error::operation_aborted ) { // do nothing, exit asap
DBG("LongPollHandler received operation_aborted: " << ex.what() ) ;
					break ;
				}
DBG("LongPollHandler received boost exception: " << ex.what() ) ;
			m_sender.fulfill_data(std::current_exception()) ; 
			break ; // exit
			} catch ( const SenderException& ex ) { // connect failed
				if ( reconnect ) {
					std::this_thread::sleep_for(std::chrono::seconds(1)) ; 
					continue ; // attempt to reconnect
				};
			}
			catch ( const std::exception& ex ) {	
DBG("LongPollHandler received exception " << ex.what() ) ;
			m_sender.fulfill_data(std::current_exception()) ; // user will know there is a problem and he needs to handle this
			break ; // exit
			}
		} // never return
	m_running = false ;
DBG("LongPollHandler exited from loop, no more polling") ;
    }) ;
}

template <class TStream>
void LongPollHandler<TStream>::stop() {
	TStream::m_threadpool.stop() ;
}

template <class TStream>
LongPollHandler<TStream>::~LongPollHandler() {
	//m_threadpool.stop() ; // already done in base class?
	//m_threadpool.join() ;
//	boost::beast::error_code ec ;
//	socket().shutdown(tcp::socket::shutdown_both, ec) ;
DBG("LongPollHandler destructor exited") ;
}

// put in library instantiations for two cases
template class LongPollHandler<TCPStream> ;
template class LongPollHandler<SSLStream> ;

}}