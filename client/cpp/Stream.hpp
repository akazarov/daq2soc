#ifndef TDAQ_DAQ2SOC_STREAM
#define TDAQ_DAQ2SOC_STREAM

#include <chrono>
#include <map>
#include <optional>
#include <memory>
#include <string>
#include <string_view>
#include <vector>
#include <tuple>
		
#include <boost/asio.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/asio/ssl.hpp>

#include "Exceptions.hpp"

namespace http = boost::beast::http ;
namespace ssl = boost::asio::ssl ;
namespace beast = boost::beast ;
namespace net = boost::asio ;

using tcp = boost::asio::ip::tcp ; 

namespace tdaq::daq2soc {

// using PayloadOctet = uint8_t ;

/** @typedef payload_byte_t
 * a data type of vector used for the request/response payload
*/
using payload_byte_t = uint8_t ;

/**
 * @typedef payload_t
 * @brief data structure for a payload passed to and from daq2soc server
 * 
 * Container of std::vector of bytes (characters). It is assumed that user code is responsible to convert
 * user data structures into a \c vector<payload_byte_t>. A simplest way would be to construct it from a string (e.g. in JSON).
 */
using payload_t = std::vector<payload_byte_t> ;

/**
 * @typedef body_t
 * @brief data structure for a http request body
 * 
 */
using body_t = http::vector_body<payload_byte_t> ;

/**
 * @typedef data_t
 * @brief Data is a \c std::tuple structure combining an integer return code from a command and a Payload
 * @see tdaq::daq2soc::payload_t
 */
using data_t = std::tuple<int, payload_t> ;

/**
 * @typedef data_sp
 * @brief Shared pointer to data_t
 * @see tdaq::daq2soc::data_t
 */
using data_sp = std::shared_ptr<data_t> ;

/** 
 * @class StreamBase
 * @brief Internal class, not directly exposed to daq2soc user API.
 * 
 * Base class holding common data and virtual methods for establishing connection
 * for two possible implementations (TCPStream and SSLStream).
 */
class StreamBase {

private: 

	StreamBase(const StreamBase&) = delete ;
    StreamBase(StreamBase&&) = delete ;
    StreamBase& operator=(const StreamBase&) = delete ;
    StreamBase& operator=(StreamBase&&) = delete ;

protected:
    boost::asio::io_context     m_ioc ;
    tcp::resolver               m_resolver ;
    std::string              	m_host_port ; // localhost:443
    std::string              	m_host ; // localhost
    std::string					m_device ; /** device name to prepend in the request URL like \c <host>:<port>/<device>/tdaq/<endpoint> */
    tcp::resolver::results_type m_endpoints ;
    boost::beast::flat_buffer   m_buffer ;
    http::response<body_t> 		m_res ;
    http::request<body_t> 		m_req ;
  	boost::asio::thread_pool 	m_threadpool ;  // thread pool, shared as I/O context by all async handlers
 
	/** Constructor, resolves host:port
	 */
	explicit StreamBase(const std::string_view& host_port) ;

	/** Constructor, resolves host:port
	 * 
	 * Also allows to specify device name
	 */
	StreamBase(const std::string_view& host_port, const std::string_view& device) ;

	virtual ~StreamBase() ;

	std::string prepare_parameters(const std::string_view& endpoint, const std::map<std::string, std::string>& parameters) ;

	virtual void connect() = 0 ;
	virtual void reconnect() = 0 ;
	virtual void set_comm_timeout(std::chrono::seconds timeout) = 0 ;
	virtual tcp::socket& socket() = 0 ; /** overload to return underlying TCP socket e.g. from SSL stream */

} ;

/** 
 * @class TCPStream
 * @brief Internal class, not directly exposed to daq2soc user API.
 * 
 * Implements HTTP stream over non-encrypted TCP connection.
 */
class TCPStream: public StreamBase {
protected:
    boost::beast::tcp_stream m_stream ;

	explicit TCPStream(const std::string_view& host_port) ;
	TCPStream(const std::string_view& host_port, const std::string_view& device) ;

	virtual void connect() override ;
	virtual void reconnect() override ;
	virtual void set_comm_timeout(std::chrono::seconds timeout) override ;
	virtual tcp::socket& socket() override { return m_stream.socket() ; } ;
	
	boost::beast::tcp_stream& stream() { return m_stream ; }  ;

	data_t sendRequestSync (std::chrono::seconds timeout) ;

	virtual ~TCPStream() ;
} ;

/** 
 * @class SSLStream
 * @brief Internal class, not directly exposed to daq2soc user API.
 * 
 * Implements HTTPS stream over SSL-encrypted TCP connection. The server certificates validation is disabled.
 */
class SSLStream: public StreamBase {
private:

protected:
    ssl::context                    						m_ssl_ctx ; 
	// ssl::stream is not copy-constructable, so one need this trick to .emplace when reconstruction is needed
	std::optional<ssl::stream<boost::beast::tcp_stream>>  	m_stream ;

	explicit SSLStream(const std::string_view& host_port) ;
	SSLStream(const std::string_view& host_port, const std::string_view& device) ;
	
	virtual void connect() override ;
	virtual void reconnect() override ;
	virtual void set_comm_timeout(std::chrono::seconds timeout) override ;
	virtual tcp::socket& socket() override { return m_stream->lowest_layer() ; }  ;

	ssl::stream<boost::beast::tcp_stream>& stream() { return m_stream.value() ; }  ;

	data_t sendRequestSync(std::chrono::seconds timeout) ;

	virtual ~SSLStream() ;
} ;

}

#endif