/**
 * @file test_sync.cpp 
 * 
 * @brief Demonstrates use of synchronous requests with tdaq::daq2soc::Sender::sendRequestSync and tdaq::daq2soc::Sender::sendRequestAsync API.
 * 
 * @see tdaq::daq2soc::Sender
 */

#include <chrono>
#include "daqsoc.hpp"

// example class Histogram as tuple
#include "common/cpp/Histo.hpp"

// for conversion of JSON into Histogram
#include "common/cpp/json/data2json.hpp"

using namespace std::chrono ;

using Histo = tdaq::daq2soc::example::Histogram<10>  ;

int main(int argc, char** argv) {

if(argc < 3) {
    std::cerr <<
        "Usage: " << argv[0] << " <host>:<port> <endpoint> [<parameter=value>]\n" <<
        "Example:\n" << argv[0] << " localhost:8080 rc command=INITIALIZE\n" ;
    return EXIT_FAILURE;
}

auto const host = argv[1] ;
auto const endpoint = argv[2] ;
std::map<std::string, std::string> parameters0 { } ;
std::map<std::string, std::string> parameters { {"partition", "ATLAS"} } ;
std::map<std::string, std::string> parameters7 { {"value", "7"} } ;
std::map<std::string, std::string> parameters5 { {"value", "5"} } ;
std::map<std::string, std::string> parameters2 { {"value", "2"} } ;

if ( argc == 4 ) {
    std::string pars(argv[3]) ;
    size_t pos = pars.find('=') ;
    if ( pos == std::string::npos ) {
        std::cerr << "Wrong parameters passed, could not parse: " << argv[3] << std::endl ;
        return EXIT_FAILURE;
    }
    parameters.emplace(pars.substr(0,pos), pars.substr(pos+1)) ;
}

try {
    tdaq::daq2soc::SenderSSL mysender { host } ;
    tdaq::daq2soc::payload_t payload { 0,1,2,3,4,5 } ; 

    // big payload
    std::vector<tdaq::daq2soc::payload_byte_t> vector_1M(1024*1024, 0xFF) ;

    // for test purpose, payload as tuple of all possible basic types
	int i = -667 ;
	unsigned int ui {15} ;
	long long z = 349857458457LL ;
	float f = 0.34345f ;
	long double ld {123.456E-635L} ;
	double d {.456E-67L} ;
	std::string strwq{"my \"string\" with \\quotes"} ;
	std::string strwoq{"my string without quotes"} ;
	std::array<float, 3> farr { 0.001, 0.002, 0.003 } ;

	auto user_data = std::tuple { i, ui, z, f, d, ld, strwq, strwoq, farr } ;

	if ( std::string(endpoint) == "bad_request" ) {
        try {
            auto res = mysender.sendRequestSync(endpoint, parameters, user_data, 2s) ;
            std::cerr << "Got response with status " << std::get<0>(res) << std::endl ;
        } catch ( const tdaq::daq2soc::UserException& ex ) {
            std::cerr << "Got exception " << ex.what() << std::endl ;
        }
        std::cerr << "Exiting" << std::endl ;
        exit(0) ;
    }

	if ( std::string(endpoint) == "get_exception" ) {
        try {
            auto res = mysender.sendRequestSync(endpoint, parameters, std::move(vector_1M)) ;
        } catch ( const tdaq::daq2soc::UserException& ex ) {
std::cerr << "Got exception from server processing: " << ex.what() << std::endl ;
        }
        exit(0) ;
    }

	if ( std::string(endpoint) == "get_histogram" ) {
		using namespace tdaq::daq2soc::example ;
		system_clock::time_point zero = system_clock::now() ;
		auto res = mysender.sendRequestSync(endpoint, parameters, payload) ;
		std::cout << " histogram received in "  << duration_cast<microseconds>(system_clock::now()-zero).count() << " microseconds" << std::endl ;
		if ( std::get<int>(res) ) {
            std::cout << "Failed: " << std::get<int>(res) << std::endl ;
            exit(1) ;
        }
		auto const& data = std::get<1>(res) ;
		std::string myhist(data.begin(), data.end()) ;
		tdaq::daq2soc::example::Histogram<10> hist = tdaq::daq2soc::json2data<tdaq::daq2soc::example::Histogram<10>>(myhist) ;
		std::cout << " histogram unpacked after "  << duration_cast<microseconds>(system_clock::now()-zero).count() << " microseconds" << std::endl ;
		std::cerr << "Got histogram: " << std::get<std::string>(hist) << std::endl ;
		for ( auto f: std::get<1>(hist) ) { std::cerr << f << "," ; } ;
		std::cerr << std::endl ;
		exit(0) ;
	}

system_clock::time_point zero = system_clock::now() ;
size_t nmsg = 150 ;

// test sync communication


std::cerr << "Sending " << nmsg << " sync messages" << std::endl ;
    for ( size_t i = 0; i < nmsg; i++ ) {
        try {
            auto res = mysender.sendRequestSync(endpoint, parameters, payload) ;
            auto const& data = std::get<1>(res) ;
            if ( std::get<int>(res) ) {
                std::cout << "Failed: " << std::get<0>(res) << std::endl ;
                continue ;
            }
            if ( !data.empty() ) {
//                std::cout << "Ret code: " << std::get<0>(res) << std::endl ;
                std::cout << i << ": payload size: " << data.size() << std::endl ;
            }
        } catch ( const tdaq::daq2soc::TimeoutException& ex ) {
            std::cerr << ex.what() << std::endl ;
            continue ;
        } catch ( std::runtime_error & ex ) {
            std::cerr << ex.what() << std::endl ;
            return 1 ;
        }
    }
    
std::cout << nmsg << " messages sent in "  << duration_cast<microseconds>(system_clock::now()-zero).count() << " microseconds" << std::endl ;

// test timeout
 	if ( std::string(endpoint) == "get_timeout" ) {
        try {
            auto res = mysender.sendRequestSync("sleep", {{"value","5"}}, payload, 2s) ;
        } catch ( const tdaq::daq2soc::TimeoutException& ex ) {
std::cerr << "Got timeout from server processing: " << ex.what() << std::endl ;
// now mysender is not connected!
        }
    }
// test pseudo-async communication

    tdaq::daq2soc::payload_t payload3 { 'a', 's', 'y', 'n', 'c' } ; 
    tdaq::daq2soc::payload_t payload2 { 'a', 's', 'y', 'n', 'c' } ; 

    std::vector<std::shared_ptr<tdaq::daq2soc::AsyncHandlerSSL>> results ;
    std::vector<std::tuple<std::future<tdaq::daq2soc::data_t>, std::shared_ptr<std::promise<tdaq::daq2soc::data_t>>>> results2 ;

    size_t count = 35 ;

    // test getData(duration)
    // sleep for 3 seconds and wait in loop
    {
    auto resfuture = mysender.sendRequestAsync("sleep", {{"value", "4"}}, payload3, 5s) ;

    std::optional<tdaq::daq2soc::data_t> res ;
    while ( !(res = resfuture->getData(std::chrono::microseconds(1))) ) { std::this_thread::yield(); } ;

    std::cerr << "Passed 4 seconds yielding in remote call" << std::endl ;
    }

    // get TimeoutException
    try { 
        auto handler = mysender.sendRequestAsync("sleep", {{"value", "13"}}, std::move(payload2), 5s) ;
        auto data = handler->getData() ;
    } catch ( const tdaq::daq2soc::TimeoutException& ex ) {
        std::cerr << "Got 5 seconds timeout" << std::endl ;
    }

zero = system_clock::now() ;
    
    for ( size_t i=0; i<count; i++ ) {
        try {
        results.emplace_back(mysender.sendRequestAsync(std::string(endpoint), parameters, payload)) ;
        } catch ( std::exception& ex ) {
            std::cerr << "Failed to send command: " << ex.what() << std::endl ;
            continue ;
        }
    } ;

    std::cout << count << " SendRequestAsync done in " << duration_cast<microseconds>(system_clock::now()-zero).count() << " microseconds " << ", getting responses ..." << std::endl ;
  
zero = system_clock::now() ;
    for ( size_t i=0; i<count; i++ ) {
        // std::cerr << i << ": waiting for data" << std::endl ;
        try { 
            auto res20s = results[i]->getData() ;
            std::cout << "Async return code: " << std::get<0>(res20s) << std::endl ;
            std::cout << "Payload: " << std::get<1>(res20s) << std::endl ;
        } catch ( const std::runtime_error& ex ) {
            std::cerr << "Failed to get result: " << ex.what() << std::endl ;
            continue ;
        }
    }
    
        std::cout << "Recieved all responces in " << duration_cast<microseconds>(system_clock::now()-zero).count() << " microseconds " << std::endl ;
     
} 
catch ( const std::exception& ex ) {
    std::cerr << "Oops: " << ex.what() << std::endl ;
}

}
