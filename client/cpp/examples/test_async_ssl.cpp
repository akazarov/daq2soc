/**
 * @file test_async.cpp 
 * 
 * @brief Demonstrates use of asynchronbous requests with @ref tdaq::daq2soc::Sender::getDataAsync API.
 * 
 * @see tdaq::daq2soc::Sender
 */

#include <chrono>

#include "daqsoc.hpp"

// example class Histogram as tuple
#include "common/cpp/Histo.hpp"

// for conversion of JSON into Histogram
#include "common/cpp/json/data2json.hpp"

using namespace std::chrono ;
using namespace tdaq::daq2soc ;

using Histo = example::Histogram<1024>  ;

void my_callback(const std::string& id, data_sp data, std::exception_ptr ex) {
    if ( ex != nullptr ) {
        try {
            std::rethrow_exception(ex) ;        
        } catch ( const std::exception& ex ) {
            std::cerr << "Got exception " << ex.what() << std::endl ;
        }
    return ;        
    }
std::cerr << "Got callback for " << id << " of size " << std::get<payload_t>(*data.get()).size() << std::endl ;
}

int main(int argc, char** argv) {

if(argc < 3) {
    std::cerr <<
        "Usage: " << argv[0] << " <host>:<port> <repetitions>\n" <<
        "Example:\n" <<  argv[0] << " localhost:443 10\n" ;
    return EXIT_FAILURE;
}

auto const host_port = argv[1] ;
int rep = strtol(argv[2], NULL, 10) ;
std::map<std::string, std::string> parameters { {"partition", "ATLAS"} } ;

try {
    tdaq::daq2soc::SenderSSL mysender { host_port } ;
    auto data_h1 = mysender.getDataAsync("histogram1024") ;
    auto data_h2 = mysender.getDataAsync("histogram1024") ;
    auto data_count = mysender.getDataAsync("counter") ;

    // callback
    mysender.getDataAsync(my_callback) ;

while (rep) {

    //auto data_f = mysender.GetDataAsync("histogram1024") ;
//std::cout << "Async get histogram sent" << std::endl ;

    //auto data_f2 = mysender.GetDataAsync("histogram1024") ;
//std::cout << "Async get histogram sent 2" << std::endl ;

    //auto data_count = mysender.GetDataAsync("counter") ;

    if ( data_h1.wait_for(std::chrono::milliseconds(1)) == std::future_status::ready ) {
std::cout << "Async get histogram received" << std::endl ;
        auto res = data_h1.get() ;
        auto const& data = std::get<1>(res) ;
        if ( !data.empty() ) {
            std::cout << "Ret code: " << std::get<0>(res) << std::endl ;
            std::cout << "Payload size: " << data.size() << std::endl ;
//            std::cout << data << std::endl ;
        }
        rep-- ;
        data_h1 = mysender.getDataAsync("histogram1024") ; // keep getting
    }

    if ( data_h2.wait_for(std::chrono::milliseconds(1)) == std::future_status::ready ) {
std::cout << "Async get histogram 2 received" << std::endl ;
        auto res = data_h2.get() ;
        auto const& data = std::get<1>(res) ;
        if ( !data.empty() ) {
            std::cout << "Ret code: " << std::get<0>(res) << std::endl ;
            std::cout << "Payload size: " << data.size() << std::endl ;
//            std::cout << data << std::endl ;
        }
        data_h2 = mysender.getDataAsync("histogram1024") ; // keep getting
    }
 
    if ( data_count.wait_for(std::chrono::milliseconds(1)) == std::future_status::ready ) {
std::cout << "Async get counter received" << std::endl ;
        auto res = data_count.get() ;
        auto const& data = std::get<1>(res) ;
        if ( !data.empty() ) {
            // std::tuple<int> cnt = tdaq::daq2soc::json2data<std::tuple<int>>(data) ;
            std::cout << "Counter: " << data << std::endl ;
        }
        data_count = mysender.getDataAsync("counter") ;
    }
//	exit(0) ;
    }

	}
catch ( const std::runtime_error& ex ) {
    std::cerr << "Oops: " << ex.what() << std::endl ;
    exit(1) ;
}

}
