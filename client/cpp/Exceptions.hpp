#ifndef TDAQ_DAQ2SOC_EXCEPTIONS
#define TDAQ_DAQ2SOC_EXCEPTIONS

#include <string>
#include <exception>
#include <mutex>
#include <chrono>
// #include <experimental/source_location>

#include <boost/beast/core.hpp>

#ifdef DEBUGME
#include <iostream>
#include <iomanip>
#include <experimental/source_location> 
static std::mutex mx ; 
#define DBG(msg) { std::time_t ts = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); \
std::lock_guard<std::mutex> lock(mx) ; \
const std::experimental::source_location loc = std::experimental::source_location::current() ;\
std::cerr << std::put_time(gmtime(&ts), "%Y-%m-%d %H:%M:%S") << " @" << loc.function_name() << \
"[" << loc.file_name() << ":" << loc.line() << "] " << msg << std::endl ; } ;
#else
#define DBG(msg)
#endif

/**
 * @namespace tdaq::daq2soc
 * 
 * Includes all classes and functions for daq-to-SoC communication library implementation
 */
namespace tdaq::daq2soc {


/** @class tdaq::daq2soc::SenderException
 * 
 *  @brief Exception thrown by @ref Sender methods, when a HTTP client error occurs. Typically encapsulates Boost::Beast exception.
 */
class SenderException: public std::runtime_error {

public:
    SenderException(const std::string_view msg, boost::beast::error_code ec): std::runtime_error(ec.message()), message{msg} {
        message.append(": ").append(ec.message()) ;
    } ;

    SenderException(const std::string_view msg, const std::exception& ex): std::runtime_error(ex.what()), message{msg}  {
        message.append(": ").append(ex.what()) ;
    } ;

    SenderException(const std::string_view msg, int errn): std::runtime_error(strerror(errn)), message{msg} {
        message.append(": ").append(strerror(errno)) ;
    } ;

    SenderException(const char* msg): std::runtime_error(msg), message{msg} {
    } ;

// override
const char* what() const noexcept override { return message.c_str() ; } ;

private:
    std::string message ;
} ;

/** @class tdaq::daq2soc::TimeoutException
 * 
 *  @brief Overloads SenderException to explicitly indicate timeout on command sending operation.
 */
class TimeoutException: public SenderException {
public: 
explicit TimeoutException(boost::system::system_error ec)
: SenderException("Timeout in performing requested operation", ec) {} 
} ;

/** @class tdaq::daq2soc::UserException
 * 
 *  @brief Exception produced by the user code on server is (re)thrown by @ref Sender#sendRequestSync
 */
class UserException: public std::runtime_error {
public: 
explicit UserException(const boost::string_view& what)
: std::runtime_error(what.to_string()) {} 
} ;

}

#endif
