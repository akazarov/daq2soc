#include <memory>
#include <cstdlib>
#include <chrono>
#include <ctime>   
#include <iostream>
#include <future>

#include <boost/beast/version.hpp>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>

#include "daqsoc.hpp"

namespace http = boost::beast::http ;

namespace tdaq {
namespace daq2soc {

template <class TStream>
AsyncHandler<TStream>::AsyncHandler(const std::string_view& host_port):
TStream(host_port), m_future{m_promised.get_future()} {
    TStream::connect() ;
DBG("AsyncHandler connected")
}

template <class TStream>
AsyncHandler<TStream>::~AsyncHandler() { 
DBG("AsyncHandler destroyed")
}

template <class TStream>
data_t AsyncHandler<TStream>::getData() {
    return m_future.get() ;
}

// Start the asynchronous operation
template <class TStream>
void
AsyncHandler<TStream>::req_async(std::chrono::seconds timeout)
{
    // Set up an HTTP POST request message

    m_req.set(http::field::host, TStream::m_host);
    m_req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    m_req.prepare_payload() ;

    boost::asio::spawn(TStream::stream().get_executor(), [this, timeout](boost::asio::yield_context yield) {

        // timeout on this stream    
        TStream::set_comm_timeout(timeout) ;

        try {
            [[maybe_unused]] auto sent = http::async_write(TStream::stream(), m_req, yield) ;
DBG("Sent " << sent << " bytes") ;
            [[maybe_unused]] auto received = http::async_read(TStream::stream(), m_buffer, m_res, yield) ;
DBG("Received " << received << " bytes") ;

            int retcode ; //= m_res.result_int() ;

            if ( m_res.result() == http::status::ok ) {
                retcode = 0 ;
            }
            if ( m_res.result() == http::status::bad_request ) {
                retcode = std::stoi(std::string(m_res.at("X-daq2soc-user-error"))) ; // may throw
            }
            if ( m_res.result() == http::status::internal_server_error ) { // user exception
DBG("user exception to rethrow")
                try {
                    m_res.at("X-daq2soc-user-exception") ;
                } catch ( const std::out_of_range& ex ) {
                    throw SenderException("Failed to get user exception: X-daq2soc-user-exception HTTP header is missing in reply") ;
                }
                throw UserException(m_res.at("X-daq2soc-user-exception")) ;
            }

            data_t d{ retcode, payload_t{std::move(m_res.body())} } ; // no copy, move the body vector
            m_promised.set_value( std::move(d) ) ;
        } catch ( const boost::system::system_error& ex ) {
            if ( ex.code() == boost::beast::error::timeout ) {
DBG("got timeout in tcp stream operation")
                m_promised.set_exception(std::make_exception_ptr(TimeoutException(ex))) ; // TimeoutException(boost::system::system_error ec)
            } else {
DBG("got boost::system::system_error exception in tcp stream operation: " << ex.what() )
                m_promised.set_exception(std::current_exception()) ; // ?
            }
        }  catch ( ... ) {
            m_promised.set_exception(std::current_exception()) ;
        }

        TStream::set_comm_timeout(std::chrono::seconds(0)) ;
    });

DBG("AsyncHandler req_async return")
    return ;    
}

// instantiation of Async Handler for two streams in the library
template class AsyncHandler<TCPStream> ;
template class AsyncHandler<SSLStream> ;

}}
