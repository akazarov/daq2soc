### C++ implementation of the client DAQ2SoC API

#### Introduction to API

This single-file library provides an interface for sending commands (requests) to a DAQ2SoC server application (implementing user server-side code), in either syncronous or asyncrounous manner. Classes declarations is found in [daqsoc.hpp](daqsoc.hpp) header (see Doxygen section at the end). There main class exposed to user that implements the API is class `tdaq::daq2soc::Sender`: it allows you to create a connection to the SoC server side and to send parametrised commands to it, including some payload. API provides few types of command sending techniques:

- `tdaq::daq2soc::Sender::sendRequestSync`  
**blocking synchronous**: the call blocks waiting for a response from the server which processes the request by calling user code.
This is done in a single TCP/HTTP connection, and all calls done by blocking `sendRequestSync` are sequentially processed on the server.
The synchronous method is preferred when one needs to send many commands that require minimal processing time on the server.

- `tdaq::daq2soc::Sender::sendRequestAsync`  
**pseudo-asynchronous** (non-blocking synchronous): it opens a dedicated connection to the server to handle this single request. The call
returns immediately a handle `tdaq::daq2soc::AsyncHandler` that can be used to get the result of request in blocking manner (featuring `C++11 future<>`).
This method can be used for command requiring some (relatiovely) long processiong time, however it must be used with care in case many commands are sent in
parallel: there may be limitations on number of open files/sockets, and also on the number of processing threads on the server.
both sendRequestSync and sendRequestAsync remotely call server API `tdaq::daq2soc::DAQRequestProcessor::processSyncRequest`. For example of use, see [test_sync.cpp](examples/test_sync.cpp).

Both methods allow to specify a timeout for command processing, in which case, the `tdaq::daq2soc::TimeoutException` is thrown. Default communicaiton timeout is set to 5 seconds.

- `tdaq::daq2soc::Sender::getDataAsync`  
**truly asynchronous**: a subscription-like communication pattern is implemented. The call does not trigger any user processing on the server side, instead user can publish arbitrary data (using `tdaq::daq2soc::AsyncDataProvider::fulfillAsyncRequest`) at any moment from a user thread started by the library at server start up. It is assumed that server application is not started in context of a TDAQ data-taking session ("partition"), it may be always running and either serving synchronous requests from TDAQ applications, or publishing data asynchronously, which may be (eventually) requested by TDAQ applications. On the client by calling overloaded `getDataAsync` you can either subscribe for a particular data by it's string ID and get a `future<>` object to get the data published by the server, or provide a callback `tdaq::daq2soc::callback_t` that will be called for all data updated by the server. For example of use, see [test_async.cpp](examples/test_async.cpp).

In case in any remote call user code generates an exception (intentionally or not), it is returned back to the client and thrown as @ref tdaq::daq2soc::UserException. Other exceptions, e.g. thrown by Boost libraries, should be caugth as `std::runtime_error` or `std::exception`.

Internally, user calls are converted into `HTTP` calls to the server, using `Boost/asio/beast` `HTTP` classes.

#### SSL version

Equivalent API is provided by `tdaq::daq2soc::SenderSSL` class, which establishes SSL (v3) handshake and communication with a server. The `daq2soc_server_ssl` binary is using self-signed certificate, so the certifical verification is disabled on client side at the moment.

#### Compiling sources and examples

To compile the daq2soc client code and the example, you need to install boost-1.75 or newer development package.
For EL9/Alma9: default version of boost is 1.75 (`dnf install boost-devel`).
If you take `boost` from other location, please modify `Makefile` accordingly.

##### Package content

*daqsoc.hpp*: - declaration of the API  
*AsyncHandler.cpp LongPollHandler.cpp Sender.cpp* - implementation that is compiled into `libdaq2soc_client.so` shared module that needs to be linked to the user binary.  
*Makefile*: simple Makefile to build all client targets: `client` for .so library and `tests` for two test binaries.  
*examples*: *test_sync.cpp* *test_async.cpp* *test_sync_ssl.cpp* *test_async_ssl.cpp*: compile it and run to send some commands (with a parameter) to a server running on port 8080/8843 on localhost:  

```bash
make -j5 all
make test
```

or run the tests manually (provided you already started a test server):

```bash
LD_LIBRARY_PATH=`pwd`/build-x86_64 ./build-x86_64/test_sync localhost:8080 rc command=INIT
LD_LIBRARY_PATH=`pwd`/build-x86_64 ./build-x86_64/test_sync localhost:8080 get_counter
LD_LIBRARY_PATH=`pwd`/build-x86_64 ./build-x86_64/test_sync localhost:8080 get_timeout
LD_LIBRARY_PATH=`pwd`/build-x86_64 ./build-x86_64/test_async localhost:8080 5

```

##### Doxygen

[Sender](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/classtdaq_1_1daq2soc_1_1_sender.html)  
[AsyncHandler](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/classtdaq_1_1daq2soc_1_1_async_handler.html)
