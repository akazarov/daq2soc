#include <string_view>

#include "Stream.hpp"

#include <boost/beast/version.hpp>
#include <boost/asio/spawn.hpp>

namespace http = boost::beast::http ;
using tcp = boost::asio::ip::tcp ; 

namespace tdaq::daq2soc {

// some default values
const std::string_view domain {"/tdaq/"} ;
const std::string_view def_service_port {"8080"} ; 
const int def_threadpool_size{4} ; // more than one thread needed to maintain multiple async connections to different sockets
const std::string_view tdaq_release {"tdaq-11-02-01"} ; // TODO get from env

// helper function to initialize thread pool
size_t
get_n_threads() {
    if ( const auto* p = getenv("DAQ2SOC_THREADPOOL_SIZE") ) {
		errno = 0 ;
        int i = strtol(p, NULL, 10) ;
		if ( i > 1024 ) {
			throw tdaq::daq2soc::SenderException("Too many threads requested") ;
		}
        if ( errno ) {
            { throw tdaq::daq2soc::SenderException("Failed to get number of threads from DAQ2SOC_THREADPOOL_SIZE environment", errno) ; }
        return static_cast<size_t>(i) ;
        }   
    }
    return tdaq::daq2soc::def_threadpool_size ;
}


/**
 * resolve host:port
 */
StreamBase::StreamBase(const std::string_view& host_port)
:m_ioc{1}, m_resolver{m_ioc}, m_host_port{host_port}, m_threadpool{get_n_threads()} {
	auto pos = host_port.find(":") ;
	m_host = host_port.substr(0UL, pos == std::string::npos ? host_port.length() : pos) ;
	std::string_view service = pos == std::string::npos ? def_service_port : host_port.substr(pos+1, host_port.length()-pos-1) ;

	boost::beast::error_code ecode ;
	m_endpoints = m_resolver.resolve(m_host, service, ecode) ;
	if ( ecode.value() != 0 ) {
		throw SenderException("Failed to resolve host name " + m_host, ecode) ;
	}

DBG("stream constructed for " << m_host_port)
}

StreamBase::StreamBase(const std::string_view& host_port, const std::string_view& device)
:StreamBase(host_port) {
	m_device = device ;
} 

StreamBase::~StreamBase() {
	m_ioc.stop() ;
	m_threadpool.stop() ;
	m_threadpool.join() ;
DBG("StreamBase stopped, no more tasks")
}

// prepare parameters string for given endpoint and user parameters and add default parameters
std::string StreamBase::prepare_parameters(const std::string_view& endpoint, const std::map<std::string, std::string>& parameters) {
	std::string ret {domain} ; // /tdaq/
	if ( !m_device.empty() ) {
		ret.insert(0, m_device).insert(0, "/") ; // /dev/tdaq/
	}
	ret.append(endpoint).append("/?release=").append(tdaq_release) ;
	if ( !parameters.empty() ) {
		for ( const auto& p: parameters ) {
			ret.append("&").append(p.first).append("=").append(p.second) ;
		}
	}
DBG("request parameters: " << ret)
	return ret ;
} 

TCPStream::TCPStream(const std::string_view& host_port)
:StreamBase(host_port), m_stream{m_threadpool} {} 

TCPStream::TCPStream(const std::string_view& host_port, const std::string_view& device)
:StreamBase(host_port, device), m_stream{m_threadpool} {} 

void
TCPStream::connect() {
	/* sync connect */
	boost::beast::error_code ecode ;
	m_stream.connect(m_endpoints, ecode) ;
	if ( ecode.value() != 0 ) {
		throw SenderException("Failed to connect to host" + m_host, ecode) ;
	}
}

void
TCPStream::reconnect() {
	boost::beast::error_code ecode ;
	// some cleanup before reconnection may be usefull..
	m_stream.socket().shutdown(tcp::socket::shutdown_both, ecode) ;
	m_stream.socket().close(ecode) ;

	m_stream.connect(m_endpoints, ecode) ;
	if ( ecode.value() != 0 ) {
		throw SenderException("Failed to connect to host " + this->m_host, ecode) ;
	}
DBG("TCPStream reconnected")	
}

void
TCPStream::set_comm_timeout(std::chrono::seconds timeout) {
	if ( timeout.count() > 0 ) {
		m_stream.expires_after(timeout) ;
	} else {
		m_stream.expires_never() ;
	}
} 
	
TCPStream::~TCPStream() {
	m_stream.close() ;
	socket().close() ;
	beast::error_code ignore ;
	socket().shutdown(tcp::socket::shutdown_both, ignore) ;
DBG("TCPStream shutdown")	
} 

SSLStream::SSLStream(const std::string_view& host_port)
:StreamBase(host_port),
m_ssl_ctx(ssl::context::sslv23) {
	try {
	// This holds the root certificate used for verification
	// m_ssl_ctx.set_default_verify_paths() ;
		m_ssl_ctx.load_verify_file("/etc/ssl/cert.pem") ;

	// Verify the remote server's certificate
	//	m_ssl_ctx.set_verify_mode(ssl::verify_peer) ;
		m_ssl_ctx.set_verify_mode(ssl::verify_none) ;
		
		m_stream.emplace(boost::asio::make_strand(m_threadpool), m_ssl_ctx) ; // constructor for optional<>
	} catch ( const beast::system_error& er ) {
		throw SenderException("Failed to load system SSL certificates", er) ;
	}
DBG("SSL stream constructed")
} 

SSLStream::SSLStream(const std::string_view& host_port, const std::string_view& device)
:SSLStream(host_port) {
	m_device = device ;
}

void
SSLStream::connect() {

	// Set SNI Hostname (many hosts need this to handshake successfully)
	if( !SSL_set_tlsext_host_name(m_stream->native_handle(), m_host.c_str()) ) {
		beast::error_code ec{static_cast<int>(ERR_get_error()), net::error::get_ssl_category() } ;
		throw SenderException("Failed to set SNI Hostname", ec) ;
	}

    /* sync connect */
    boost::beast::error_code ecode ;
    beast::get_lowest_layer(m_stream.value()).connect(m_endpoints, ecode) ;
    if ( ecode.value() != 0 ) {
        throw SenderException("Failed to connect to host " + m_host, ecode) ;
    }
DBG("SSL Connection done")    

    m_stream->handshake(ssl::stream_base::client, ecode) ;
    if ( ecode.value() != 0 ) {
DBG("SSL handshake failed")
        throw SenderException("Failed to make SSL handshake with host " + m_host, ecode) ;
    }
DBG("SSL handshake done")
}

void
SSLStream::reconnect() {
	// this is sort of re-initialization of m_stream, see https://groups.google.com/g/boost-list/c/KNpE8Nl9qQ4/m/b3TgyLTdAgAJ
	m_stream.emplace(boost::asio::make_strand(m_threadpool), m_ssl_ctx) ;
	connect() ; // throw SenderException
DBG("SSL stream reconnected")
}

void
SSLStream::set_comm_timeout(std::chrono::seconds timeout) {
	if ( timeout.count() > 0 ) {
		beast::get_lowest_layer(m_stream.value()).expires_after(timeout) ;
	} else {
		beast::get_lowest_layer(m_stream.value()).expires_never() ;
	}
}
	
SSLStream::~SSLStream() {
	try {
	//	m_stream.value().shutdown() ;
		beast::error_code ignore ;
		socket().close() ;
		socket().shutdown(tcp::socket::shutdown_both, ignore) ;
	} catch ( const std::exception& ex ) {
DBG("exception in SSL shutdown: " << ex.what())		
	}

DBG("SSL stream shutdown")
} ;

data_t
TCPStream::sendRequestSync(std::chrono::seconds timeout) {

    connect() ; // just reconnet if connection is dropped

DBG("Sending to " << m_req.target())

    m_req.set(http::field::host, m_host) ;
    m_req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING) ;

//    req.keep_alive(true) ; ?

    m_req.prepare_payload() ;

    // Send the HTTP request to the remote host
    int retcode {-1} ;
    std::exception_ptr eptr ;

    boost::asio::spawn(m_ioc, [&](boost::asio::yield_context yield) {
        try {
            set_comm_timeout(timeout) ;

            [[maybe_unused]] auto sent = http::async_write(m_stream, m_req, yield) ;            
DBG("Sent " << sent << " bytes") ;
            [[maybe_unused]] auto received = http::async_read(m_stream, m_buffer, m_res, yield) ;
DBG("Received " << received << " bytes") ; 

//            [[maybe_unused]] boost::optional< std::uint64_t > ps = m_res.payload_size() ;
DBG("response received: " << m_res.result_int())

        } catch ( ... ) {
            eptr = std::current_exception() ;
        }

        set_comm_timeout(std::chrono::seconds(0)) ;
    }) ;

    m_ioc.run() ; // wait for write/read to complete, or timeout
    m_ioc.restart() ;
    
    if ( eptr ) {
        try {
            std::rethrow_exception(eptr);
        }
        catch (const boost::system::system_error& ex) {
            if ( ex.code() == boost::beast::error::timeout ) {
DBG("got timeout from async operation")
                throw TimeoutException(ex) ;
            } else {
                throw SenderException("boost::system::system_error in TCP stream read/write: ", ex) ;
            }
        } catch ( const std::exception& ex ) {
DBG("std::exception: " << ex.what())
            throw SenderException("std::exception in TCP stream read/write: ", ex) ;
        }
    }

    if ( m_res.result() == http::status::ok ) {
        retcode = 0 ;
    }
    if ( m_res.result() == http::status::bad_request ) { // non-0 status from server user processing
        retcode = std::stoi(std::string(m_res.at("X-daq2soc-user-error"))) ; // throw
    }
    if ( m_res.result() == http::status::internal_server_error ) { // user exception
        //auto what = std::string(resp.at("X-daq2soc-user-exception")) ;
DBG("user exception to rethrow")
        throw UserException(m_res.at("X-daq2soc-user-exception")) ;
    }

    return data_t { retcode, payload_t{std::move(m_res.body())} } ;
}


data_t
SSLStream::sendRequestSync(std::chrono::seconds timeout) {
    m_req.set(http::field::host, m_host) ;
    m_req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING) ;
    m_req.keep_alive(true) ;

    m_req.prepare_payload() ;

    // Send the HTTP request to the remote host
    int retcode {-1} ;
    std::exception_ptr eptr ;

    boost::asio::spawn(m_ioc, [&] (boost::asio::yield_context yield) {
        
        size_t reconnect {0} ;
        while ( true ) {
            try {
                if ( reconnect ) {
DBG("Socket is open: " << m_stream->lowest_layer().is_open())

                    //boost::beast::error_code ignore ; // does not work
                    //m_stream.lowest_layer().shutdown(tcp::socket::shutdown_both, ignore) ;
                    //m_stream.lowest_layer().close() ;
                    // m_stream.shutdown() ; too bad
                    // this is sort of re-initialization of m_stream, see https://groups.google.com/g/boost-list/c/KNpE8Nl9qQ4/m/b3TgyLTdAgAJ
                    m_stream.emplace(boost::asio::make_strand(m_threadpool), m_ssl_ctx) ;

                    connect() ; // throw SenderException
DBG("Reconnected") 
                    reconnect++ ;
                }

                set_comm_timeout(timeout) ;

DBG("Async write starting")
                [[maybe_unused]] auto sent = http::async_write(m_stream.value(), m_req, yield) ;            
DBG("Sent " << sent << " bytes") ;
                [[maybe_unused]] auto received = http::async_read(m_stream.value(), m_buffer, m_res, yield) ;
DBG("Received " << received << " bytes") ;
                break ;
            } 
            catch ( boost::system::system_error const& ex ) {  // handle end_of_stream specifically
                if ( ex.code() == http::error::end_of_stream ) {
                    if ( reconnect > 1 ) {
DBG("Too many reconnects, giving up") ;
                        eptr = std::current_exception() ;
                        break ;
                    }
DBG("Connection closed by peer, will reconnect") ;
                    // m_stream.shutdown() ;
                    reconnect = 1 ;
                    continue ;
                }
                eptr = std::current_exception() ; // e.g. timeout
                break ;
            }
            catch ( ... ) {
                eptr = std::current_exception() ;
                break ;
            }
        } // while reconnect

        set_comm_timeout(std::chrono::seconds(0)) ;
    }); // spawn

    m_ioc.run() ;
    m_ioc.restart() ;

    if ( eptr ) {
        try {
            std::rethrow_exception(eptr) ;
        }
        catch ( boost::system::system_error const& ex ) {
            if ( ex.code() == boost::beast::error::timeout ) {
DBG("got timeout from async operation")
                throw TimeoutException(ex) ;
            }
            throw SenderException("boost::system::system_error in read/write: ", ex) ;
        }
        catch ( const std::exception& ex ) {
DBG("std::exception: " << ex.what())
            throw SenderException("std::exception in read/write: ", ex) ;
        }
    }

DBG("response received: " << m_res.result_int() << ": " << retcode << " bytes with payload " << *m_res.payload_size())

    // cook the return data {code, payload}
    try {
        if ( m_res.result() == http::status::ok ) {
            retcode = 0 ;
        } else
        if ( m_res.result() == http::status::bad_request ) {
            retcode = std::stoi(std::string(m_res.at("X-daq2soc-user-error"))) ; // throw
        } else
        if ( m_res.result() == http::status::internal_server_error ) { // user exception
            //auto what = std::string(resp.at("X-daq2soc-user-exception")) ;
    DBG("user exception to rethrow")
            throw UserException(m_res.at("X-daq2soc-user-exception")) ;
        }
    } catch ( const std::exception& ex ) {
        throw SenderException("failed to get expected response headers", ex) ;
    }
 
    return data_t { retcode, payload_t{std::move(m_res.body())} } ;
}

} // tdaq::daq2soc