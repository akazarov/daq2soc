#include <memory>
#include <cstdlib>
#include <chrono>
#include <ctime>   
#include <iostream>
#include <future>

#include <boost/beast/version.hpp>
#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio/ssl.hpp>

#include "daqsoc.hpp"

namespace tdaq {
namespace daq2soc {

template <class TStream>
Sender<TStream>::Sender(const std::string_view& host_port) 
:TStream(host_port), m_cb_threadpool{4} {
    TStream::connect() ;
}

template <class TStream>
Sender<TStream>::Sender(const std::string_view& host_port, const std::string_view& device) 
:Sender(host_port) {
TStream::m_device = device ;
}

template <class TStream>
Sender<TStream>::~Sender() {
// m_stream.close() ; // ? needed
beast::error_code ignore ; //moved to Base
TStream::socket().shutdown(tcp::socket::shutdown_both, ignore) ;
if ( m_lph ) {
    m_lph->stop() ;
    m_lph.reset() ; // call destructor of LPH so that it's thread pool is stopped and no callbacks are processed
}
m_cb_threadpool.join() ;
DBG("Sender destroyed")
}

template <class TStream>
void Sender<TStream>::fulfill_data(const std::string& id, data_sp data) {
    {
    std::lock_guard<std::mutex> lock(m_mutex) ; 
    for ( auto& p_data: m_async_promised_data[id] )  {
        try {
DBG("fulfilling data (move) for " << id)
            p_data.set_value(*data.get()) ; // copy?
        } catch (const std::future_error& ex) {
DBG("future_error in fulfilling data: " << ex.what())
        }
    }
    m_async_promised_data.erase(id) ;
    }

    for ( auto& cb: m_user_callbacks ) {
DBG("calling user callback for async data " << id)
        boost::asio::post(m_cb_threadpool, std::bind(cb, id, data, nullptr)) ;
    }
}

template <class TStream>
void Sender<TStream>::fulfill_data(const std::exception_ptr ex) {
    std::lock_guard<std::mutex> lock(m_mutex) ;
    for ( auto& d: m_async_promised_data ) {
        for ( auto& l: d.second )  {
            try {
                l.set_exception(ex) ;
            } catch (const std::future_error& fex) {
DBG("future_error in fulfilling data: " << fex.what())
            }
        }
    }
    m_async_promised_data.clear() ;
    
    for ( const auto& cb: m_user_callbacks ) {
DBG("calling user callback with exception")
        cb("", {}, ex) ;
    }
}

/** 
 * @brief Implements asynchronous communication to the server, getting a user data by ID.
 * 
 * The server user code eventually publishes some data (in asyncRequestProcessor#fulfillAsyncRequest), using an ID.
 * A new data is checked continueusly (blocking on a future) in a thread that accepted a long polling request (internal LongPollHandler class)
 * from the client that started once per Sender-Server pair.
*/
template <class TStream>
std::future<data_t>
Sender<TStream>::getDataAsync ( const std::string& id ) {

    std::promise<data_t> data_promised ;
    std::future<data_t> data_future = data_promised.get_future() ;

    std::lock_guard<std::mutex> lock(m_mutex) ; 
    if ( auto it = m_async_promised_data.find(id) ; it != m_async_promised_data.end() ) {
DBG("more then one client requested async data with ID " << id)
        it->second.emplace_back(std::move(data_promised)) ;
        // m_async_promised_data.erase(it) ;
    } else {
        std::list<std::promise<data_t>> l; l.emplace_back(std::move(data_promised)) ;
        m_async_promised_data.emplace(id, std::move(l) ) ;
DBG("client requested async data with ID " << id)
    }

    // lazy start a long poll async handler which will eventually fulfill the promise
    if ( !m_lph ) {
        m_lph = std::shared_ptr<LongPollHandler<TStream>>(new LongPollHandler<TStream>(TStream::m_host_port, *this)) ;
    }
    m_lph->run() ;

    return data_future ;
}

/**
 * @brief Implements asynchronous communication to the server, getting all user data pushed from server in a callback.
 * 
 * The server user code eventually publishes some data (in asyncRequestProcessor#fulfillAsyncRequest), using an ID.
 * A new data is checked continueusly (blocking on a future) in a thread that accepted a long polling request (internal LongPollHandler class)
 * from the client that started once per Sender-server pair.
 * 
 * @see @ref callback_t
 */
template <class TStream>
void
Sender<TStream>::getDataAsync ( callback_t callback ) {
    m_user_callbacks.emplace_back(callback) ;
    std::lock_guard<std::mutex> lock(m_mutex) ;
    if ( !m_lph ) {
        m_lph = std::shared_ptr<LongPollHandler<TStream>>(new LongPollHandler<TStream>(TStream::m_host_port, *this)) ;
    }
    m_lph->run() ;
}

// implementation for two streams compiled in the library
template class Sender<TCPStream> ;
template class Sender<SSLStream> ;

}}
