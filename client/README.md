### DAQ2SoC client libraries

This folder contains implementations of client DAQ2SoC API in supported languages. You can use any `HTTP` library available in your environemnt. For C++ the suggested implementation is based on [`Boost/beast`](https://www.boost.org/doc/libs/1_75_0/libs/beast/doc/html/beast/design_choices.html) single-header HTTP implementation.

##### Package content:
[cpp](cpp): files for `C++` client API based on `Boost/beast` `HTTP` implementation.

[examples](examples): source code for binaries that demonstarate use of client API, synchronous and asynchronous.

#### curl examples
Assuming the server is running on localhost, port 8080 (a parameter of the daq2soc_server).

Check the healthness of the server:

```bash
curl -i -X HEAD 'localhost:8080/tdaq/probe'
```

Get the value of an atomic counter from server, and start a thread which executes user code which increments the counter every 1 second:

```bash
curl -i -X POST 'localhost:8080/tdaq/get_counter'
curl -i -X POST 'localhost:8080/tdaq/get_histogram'
```

Reset the counter:

```bash
curl -i -X POST 'localhost:8080/tdaq/reset_counter'
```

Get a bad status from remote (HTTP BAD_REQUEST):

```bash
curl -i -X POST 'localhost:8080/tdaq/bad_request'

HTTP/1.1 400 Bad Request
Server: SOC
X-daq2soc-user-error: 1
Content-Type: text/html
Content-Length: 0
```

Get an exception from remote user code execution (HTTP INTERNAL_SERVER_ERROR):

```bash
curl -i -X POST 'localhost:8080/tdaq/get_exception'

HTTP/1.1 500 Internal Server Error
Server: SOC
Content-Type: text/html
X-daq2soc-user-exception: Bad things happen, it's your turn to handle it now
Content-Length: 92
```

