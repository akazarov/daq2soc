#include <tuple>
#include <cstdlib>
#include <string>
#include <array>

/** @example Histo.hpp 
* @brief Example class representing Histogram as a C++ tuple
*
* Instances of this class can be serialized to JSON string and deserialized back to C++ instance of Histogram by data2json() and json2data() daq2soc helper library.
* @see tdaq::daq2soc::data2json() 
* @see tdaq::daq2soc::json2data()
*/

namespace tdaq::daq2soc::example {

/**
 * @class Histogram
 * An example class for Histogram as a tuple, allowing easy packing/unpacking to/from JSON format.
 * 
 * Used in examples (@ref tdaq::daq2soc::UserExample#processSyncRequest() as example of processing "get_histogram" command),
 * and in client/cpp/examples/test_sync.cpp for de-serializing it on the client side.
 * 
 * @tparam N number of bins in histogram
 * @tparam T type of bin values (default is float)
 */
template <size_t N = 100, typename T = float>
using Histogram = std::tuple<std::string, std::array<T, N>> ;

template <size_t N = 100, typename T = float>
using Histogram2D = std::tuple<std::string, std::array<T, N>, std::array<T, N>> ;

}

