#include <string>
#include <string_view>
#include <charconv>
#include <array>
#include <algorithm>
#include <typeinfo>
#include <sstream>
#include <limits>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <vector>
#include <type_traits>
#include <typeinfo>
#include <cxxabi.h>
#include <cstdint>
// C++17 required
#include <charconv>

namespace tdaq::daq2soc {
	using payload_t = std::vector<uint8_t> ;
}

namespace tdaq::daq2soc::internal {

using tdaq::daq2soc::payload_t ;

template <typename T, typename Enable = void>
T& data2json_v(T& r) {
	return r ;
}

// may be more standard, but less compact and not really cross-platform...
static 
const char* demangle(const char* source) {
	return source ;
/*	int     status ;
	const char* ret = abi::__cxa_demangle(source, 0, 0, &status) ;
	if ( status ) {
		throw std::runtime_error("Failed to demangle C++ symbol " + std::string(source)) ;
	}
	return ret ; */
}

// for integral types only, does not work reliable for float, need separate function
template <typename Number, std::enable_if_t<std::is_integral<Number>::value, bool> = true> 
std::string& data2json(std::string& r,  const Number& value) {
	std::array<char, std::numeric_limits<Number>::digits10+2> str ;
    if  ( auto [ptr, ec] = std::to_chars(str.data(), str.data() + str.size(), value); ec == std::errc() ) {
		r.append("{\"").append(demangle(typeid(Number).name())).append("\": ").append(std::string_view(str.data(), ptr - str.data())).append("}") ;
	} 
	else {
		throw std::runtime_error(std::string("Failed to convert to string a value of ") + std::to_string(value) + ": " + ptr) ;
	}
 	return r ;
}

// for numeric integral types, where std::to_chars defined only in gcc11
template <typename Number, std::enable_if_t<std::is_floating_point<Number>::value, bool> = true> 
std::string& data2json(std::string& r,  const Number& value) {
       std::ostringstream os ;
       os << std::scientific << value ;
       r.append("{\"").append(demangle(typeid(Number).name())).append("\": ").append(os.str()).append("}") ;
       return r ;
}

// C-stype arrays
template <typename T, size_t N>
std::string& data2json(std::string& r, T (&array)[N]) {
	r.append("[") ;
    for (size_t i = 0; i < N; ++i) {
        data2json(r, array[i]) ;
		if ( i < N-1 ) { r.append(", ") ; }
    }
	r.append("]") ;
	return r ;	
} 

// std::array
template <typename T, size_t N>
std::string& data2json(std::string& r, const std::array<T,N>& array) {
	r.append("[") ;
    for (size_t i = 0; i < array.size(); ++i) {
        data2json(r, array[i]) ;
		if ( i < array.size()-1 ) { r.append(", ") ; }
    }
	r.append("]") ;
	return r ;
}

// JSON escape for '"' and '\'
static
std::string sanitize(const std::string_view& sv) {
	std::string ret ;
	ret.reserve(sv.size()) ; // + ?
	size_t e, s = 0 ;
	while ( (e = sv.find_first_of("\"\\", s)) != std::string_view::npos ) {
		ret.append(sv.substr(s, e - s)).append("\\").push_back(sv.at(e)) ;
		s = e+1 ;
	}
	ret.append(sv.substr(s, e - s)) ;
	return ret ;
}

// specification for string std::enable_if_t<std::is_integral<Integer>::value, bool> = true
template <typename String, std::enable_if_t<std::is_same<String, std::string_view>::value, bool> = true >
std::string& data2json(std::string& r, const String& v) {
	
	return r.append("{\"").append("sS").append("\": \"").append(sanitize(v)).append("\"}") ;
	// for recent C++ compilers, instead of sS for std::string type, NSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE is returned
	// which seems to be too long
	// return r.append("{\"").append(demangle(typeid(v).name())).append("\": \"").append(v).append("\"}") ;
}

static
std::string data2json(std::string& r, const char* s) {
	return data2json(r, std::string_view(s)) ;
}

static
std::string data2json(std::string& r, const std::string&s) {
	return data2json(r, std::string_view(s)) ;
}

// specification for float/double, as other ways for double do not do the right thing
/*
template <typename Double, std::enable_if_t<std::is_floating_point<Double>::value, bool> = true >
std::string& data2json(std::string& r, const Double& v) {
	std::array<char, std::numeric_limits<Double>::digits10+2> str;
	std::cerr << "Converting " << v << std::endl ;
    if  ( auto [ptr, ec] = std::to_chars(str.data(), str.data() + str.size(), v); ec == std::errc() ) {
		r.append("{\"").append(typeid(v).name()).append("\": ").append(std::string_view(str.data(), ptr - str.data())).append("}") ;
	} else {
		std::cerr << "Digits reserved: " << std::numeric_limits<Double>::digits10+2 << std::endl ;
		throw std::runtime_error(std::string("Failed to convert to string a float value of ") + std::to_string(v) + ": " + ptr) ;
	}
	return r ;
} */

/*
template <>
std::string& data2json(std::string& r, const long double& v) {
	std::array<char, std::numeric_limits<long double>::digits10+2> str;
    if  ( auto [ptr, ec] = std::to_chars(str.data(), str.data() + str.size(), v, std::chars_format::scientific, 10); ec == std::errc() ) {
		r.append("{\"").append(typeid(v).name()).append("\": ").append(std::string_view(str.data(), ptr - str.data())).append("}") ;
	} else {
		throw std::runtime_error(std::string("Failed to convert to string a value of ") + std::to_string(v) + ": " + ptr) ;
	}
	return r ;
} */

// variadic recursive template function
template<typename T, typename ... Args>
std::string& data2json(std::string& r, T const& value, Args ... args) {
	data2json(r, value) ;
	if ( sizeof...(args) ) {
		r.append(", ") ;
		data2json(r, args...) ;
	}
	return r ;
}

template<typename T, typename ... Args>
void data2json_v(payload_t& r, T const& value, Args ... args) {
	data2json_v(r, value) ;
	if ( sizeof...(args) ) {
		r.push_back(',') ;
		data2json_v(r, args...) ;
	}
}

// helper for tuple expansion into parameters pack
template<class Tuple, std::size_t... Is>
std::string data2json_tuple(const Tuple& tuple, std::index_sequence<Is...> seq ) {
	std::string ret {"["} ;
	internal::data2json(ret, std::get<Is>(tuple)...) ;
	ret.append("]") ;
	return ret ;
}

// helper for tuple expansion into parameters pack
template<class Tuple, std::size_t... Is>
payload_t data2json_tuple_v(const Tuple& tuple, std::index_sequence<Is...> seq ) {
	payload_t ret {'['} ;
	internal::data2json_v(ret, std::get<Is>(tuple)...) ;
	ret.push_back(']') ;
	return ret ;
} 

/**********************/

template<typename Integer, typename Enable = void>
struct convert_to_value {
	Integer operator()(const std::string_view& str) const {
		Integer ret ;
		auto result = std::from_chars(str.data(), str.data() + str.size(), ret);
		if (result.ec == std::errc::invalid_argument || result.ec == std::errc::result_out_of_range) {
			throw std::runtime_error("Failed to parse") ;
		}
		return ret ;
	}
};

template<typename Number>
struct convert_to_value<Number, typename std::enable_if<std::is_integral<Number>::value>::type > {
	Number operator()(const std::string_view& str) const {
		Number ret ;
		auto result = std::from_chars(str.data(), str.data() + str.size(), ret);
		if (result.ec == std::errc::invalid_argument || result.ec == std::errc::result_out_of_range) {
			throw std::runtime_error(std::string("Failed to parse to value of ") + typeid(Number).name() + " at: " + result.ptr) ;
		}
		return ret ;
	}
};

template<typename Number>
struct convert_to_value<Number, typename std::enable_if<std::is_floating_point<Number>::value>::type> {
       Number operator()(const std::string_view& str) const {
               Number ret ;
               std::string s(str) ;
               std::istringstream is(s) ;
               is >> ret ;
               return ret ;
       }
};

template<class String>
struct convert_to_value<String, typename std::enable_if<std::is_same<std::string, String>::value>::type > {
	std::string operator()(const std::string_view& str) const {
		std::string r{str.substr(1,str.length()-2)} ;
		r.erase( std::remove(r.begin(), r.end(), '\\'), r.end() ) ;
		return r ; 
	}
};

template <typename T, size_t N = 0>
struct atype{};

template <typename T, size_t N>
std::array<T,N> _json2dataPop(atype<std::array<T,N>>, const std::string_view& json, size_t* next) {
	if ( json.find_first_of('[', *next) == std::string_view::npos ) {
		throw std::runtime_error(std::string("expected [ for array, not found in: ") +
			std::string(json) + std::string(" starting from pos ") + std::to_string(*next)) ;
	}
	std::array<T,N> ret ;
	for ( size_t i = 0 ; i<N; i++) {
		ret[i] = _json2dataPop(atype<T>{}, json, next) ; // TODO check the actual length...
	}
	return ret ; // *next is pointing to either ',' (next object following the array will be parsed) or npos
}

// extracts a single object {"<type>": value} , pointed to by *next
template <typename T>
T _json2dataPop(atype<T>, const std::string_view& json, size_t* next) {
	size_t i1 = json.find_first_of('{', *next) ;
	if ( json[i1] == '{' ) { // object
		size_t i2 = json.find_first_of('}', i1) ; // TODO handle misformatting
		std::string_view first_obj = json.substr(i1+1,i2-i1-1) ;// extract object "<type>": value without braces

		*next = json.find_first_of(',', i2) ; // move index to next

		std::string_view type = first_obj.substr(1,first_obj.find('"',2)-1) ;

		if ( type != "sS" && type != typeid(T).name() ) {
			throw std::runtime_error("wrong type in JSON " + std::string(type) + ", expected from the code: " + typeid(T).name()) ;
		} 

		return convert_to_value<T>()(first_obj.substr(first_obj.find(' ',3)+1)) ; // from space-separator to the end
	} else { // [{}, {}, ...] array of objects, assuming T = std::array<P>
		throw std::runtime_error(std::string("Expected JSON object starting with {, found: ") + std::string(json.substr(i1))) ;
		// size_t i2 = json.find_first_of(']', i1) ;
	}
}

template <typename T>
T json2dataPop(const std::string_view& json, size_t* next) {
	return _json2dataPop(atype<T>{}, json, next) ; // T may be std::array<P,N>

/*
	size_t i1 = json.find_first_of("{[", *next) ;
	if ( json[i1] == '{' ) { // object
		return json2dataPop(json, next) ;
	} else { // array
		return json2dataPop(json, next) ;
	}
*/
}

template <typename... DataObjects>
struct Json2dataHelper {
static std::tuple<DataObjects...> json2data(const std::string_view& json) {
	size_t next = 1 ; // skip the leading '['
	return std::tuple<DataObjects...>{ internal::json2dataPop<DataObjects>(json, &next)... } ;
} } ; 

// for non-tuple, like basic types and std::arrays
template <typename T>
struct Json2dataHelper<T> {
static T json2data(const std::string_view& json) {
	size_t next = 1 ;
	//if ( json.find(",") != std::string::npos ) {
		// std::cerr << "converting to " << typeid(T).name() << std::endl ;
		//throw std::runtime_error(std::string("Expected single object of type ") + typeid(T).name()) ;
	//}
	return json2dataPop<T>(json, &next) ;
} } ;

template <typename... DataObjects>
struct Json2dataHelper<std::tuple<DataObjects...>> {
static std::tuple<DataObjects...> json2data(const std::string_view& json) {
	return Json2dataHelper<DataObjects...>::json2data(json) ;
} } ;

} // internal namespace
