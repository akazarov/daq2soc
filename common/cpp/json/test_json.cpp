#include <iostream>
#include <vector>
#include <tuple>
#include <string>

#include "data2json.hpp"
#include "../Histo.hpp"

/** 
 * @example test_json.cpp
 * Example on how to use the @ref tdaq::daq2soc::data2json and @ref tdaq::daq2soc::json2data helper functions
 * 
 * The code demonstrates how to convert a number of data variables and arrays, or tuples to and from JSON string.
 * It can be used for example for packing user data to a string (and then to vector<char>) in \ref tdaq::daq2soc::UserExample::processSyncRequest() when returning data to the client.
 */

int main() {

	std::array<float, 3> far { 0.840188, 0.002, 0.003 } ;
	auto json2 = tdaq::daq2soc::data2json(far) ;

	tdaq::daq2soc::example::Histogram<1024> hist { "random histogram 1024 bins", {} } ;
	std::for_each(std::begin(std::get<1>(hist)), std::end(std::get<1>(hist)), [](float& x) { x = (float)std::rand()/RAND_MAX ;}) ;
	std::string js = tdaq::daq2soc::data2json(hist) ;

	int i = -667 ;
	auto json = tdaq::daq2soc::data2json(i) ;
	std::cerr << "json: " << json << std::endl ;
	std::cerr << "value: " << tdaq::daq2soc::json2data<int>(json) << std::endl ;
	std::cerr << "json: " << tdaq::daq2soc::data2json(349857458457LL) << std::endl ;
	std::cerr << "value: " << tdaq::daq2soc::json2data<long long>(tdaq::daq2soc::data2json(349857458457LL)) << std::endl ;
	std::cerr << "json: " << tdaq::daq2soc::data2json("string literal") << std::endl ;
	std::cerr << "value: " << tdaq::daq2soc::json2data<std::string>(tdaq::daq2soc::data2json("string literal")) << std::endl ;

	unsigned int ui = 15 ;
	long long z = 349857458457LL ;
	float f = 0.34345f ;
	long double ld = 123.456E-635L ;
	double d = 0.456E-67L ;
	std::string strwq{"my \"string\" with \\quotes"} ;
	std::string strwoq{"my string without quotes"} ;
	std::array<float, 3> farr { 0.001, 0.002, 0.003 } ;
	json = tdaq::daq2soc::data2json(i, ui, z, f, d, ld, strwq, strwoq, farr) ;

	std::cerr << json << std::endl ;
// expected output:
	// [{"i": -667}, {"j": 15}, {"x": 349857458457}, {"f": 3.434500e-01}, {"d": 4.560000e-68}, {"e": 1.234560e-633},
	// {"sS": "my \"string\" with \\quotes"}, {"sS": "my string without quotes"}, [{"f": 1.000000e-03}, {"f": 2.000000e-03}, {"f": 3.000000e-03}]]

	auto tup = tdaq::daq2soc::json2data<int, unsigned int, long long, float, double, long double, std::string, std::string, std::array<float, 3>>(json) ;
	std::cerr << std::get<0>(tup) << std::endl ;
	std::cerr << std::get<long long>(tup) << std::endl ;
	std::cerr << std::get<long double>(tup) << std::endl ;
	std::cerr << std::get<float>(tup) << std::endl ;
	std::cerr << std::get<6>(tup) << std::endl ;
	std::cerr << std::get<7>(tup) << std::endl ;
	std::cerr << std::get<std::array<float, 3>>(tup)[0] << std::endl ;

	int intarr[] = { 1,2,3,4 } ;

	std::string jar = tdaq::daq2soc::data2json(intarr) ;
	std::cerr << jar << std::endl;
// output:	
// [[{"i": 1}, {"i": 2}, {"i": 3}, {"i": 4}]]	
	
	auto back = tdaq::daq2soc::json2data<std::array<int,4>>(jar) ;

	// NB: C-style T array[N] is reconstructed as std::array<T,N>
	std::cerr << "std::array of size " << back.size() << std::endl ;

// this is case of recent C++ typeinfo, "sS" for strings was returned in e.g. gcc 4.8.5
/*
	std::string_view json_input = R"foo(
	[{"i": -667}, {"j": 15}, {"x": 349857458457}, {"f": 0.001000}, {"e": 1.2345600000e-65}], {"NSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE": "my string"} ]
	)foo" ;*/

	std::string_view json_input = R"(
	[{"i": -667}, {"j": 15}, {"x": 349857458457}, {"f": 0.001000}, {"e": 1.2345600000e-65}], {"sS": "my string"} ]
	)" ;

	// get data back as a tuple of values of known types
	// arrays are not yet supported
	auto t = tdaq::daq2soc::json2data<int, unsigned int, long long, float, long double, std::string>(json_input) ;
	
	std::cerr << std::get<0>(t) << ", " << std::get<1>(t) << ", " << std::get<2>(t) << ", " << std::get<3>(t) <<
	", " << std::get<4>(t) << ", " << std::get<5>(t) << std::endl ;
	
	// if you prefer std::tuple to trivial C struct {}
	// define in some common header
	using MyData = std::tuple<std::string, int, double> ;

	MyData data_out { "counter A", 0, 0.0 } ; 

	std::string mydata_as_json = tdaq::daq2soc::data2json(data_out) ;	// as tuple
	std::string mydata_as_json2 = tdaq::daq2soc::data2json("counter B", 1, 0.1) ; // construct in place

	std::cerr << mydata_as_json << std::endl ; // or transfer it over http
	std::cerr << mydata_as_json2 << std::endl ; // or transfer it over http
	
	// get the sata as json string, deserialize it in the same type of tuple
	auto data_in = tdaq::daq2soc::json2data<std::string, int, double>(mydata_as_json) ; // use the same order of types

	auto data_in_2 = tdaq::daq2soc::json2data<MyData>(mydata_as_json2) ; // don't need to know details 

	// get fields from tuple by type (or by index)
	std::cerr << "Counter: " << std::get<std::string>(data_in) << "; int value: " << std::get<int>(data_in) << std::endl ;
	std::cerr << "Counter 2: " << std::get<std::string>(data_in_2) << "; float value: " << std::get<2>(data_in_2) << std::endl ;

	auto ddd = tdaq::daq2soc::json2data<std::string, std::array<int,3>, std::string>(R"(
	[{"sS": "counter of"}, [{"i": 1}, {"i": 2}, {"i": 1}], {"sS": "another string"}]
	)") ;
	std::cerr << "Got array: " << std::get<1>(ddd).at(0) << std::get<2>(ddd) << std::endl ;
}