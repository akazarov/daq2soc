#ifndef TDAQ_DAQ2SOC_DATATOJSON_HPP
#define TDAQ_DAQ2SOC_DATATOJSON_HPP

/**
 * @file data2json.hpp 
 * @brief Defines helper functions for converting arbitrary C++ tuple structures into a JSON string and back to C++ tuples.
 * 
 * \warning C++17 support is required in gcc. Default compiler on EL8/9 is OK.
 */

#include <utility>
#include <vector>
#include "internal/daq2soc_json.hpp"


#ifndef __cpp_lib_to_chars
// this  is fully implemented in gcc 11 only, so for CC8 default compilers this check fails
// # error "C++17 library support for to_chars/from_chars required"
#endif

#ifndef __cpp_lib_string_view
# error "C++17 library support for std::string_view required"
#endif

#ifndef __cpp_fold_expressions
# error "C++17 support for ... fold expressions required"
#endif

/**
 * @file data2json.hpp
 * 
 * @namespace tdaq::daq2soc
 * 
 * Includes all classes and functions for daq-to-SoC communication library implementation
 * 
 */
namespace tdaq { 
namespace daq2soc {

using payload_t = std::vector<uint8_t> ;

/** Function to serialize a tuple-like C++ data into a JSON string
 * 
 * Accepts arbitrary number of arguments of basic types and arrays (both std::array and plain old C arrays).
 * Converts input into an array of JSON sequence like [ {"<type>": value}, [{}, ...], ... ]
 * where <type> can be used to decode it back to a C++ variable.
 * Internally uses std::to_chars for formatting of numbers. C++17 support required.
 * Both T array[N] and std::array<T,N> are converted to the same JSON array.
 * Example:
 * 
 * @code
    using MyData = std::tuple<std::string, int, double> ;
	MyData data_out { "counter A", 0, 0.0 } ; 
	auto mydata_as_json = tdaq::daq2soc::data2json(data_out) ;	
	auto mydata_as_json2 = tdaq::daq2soc::data2json("counter B", 1, 0.1) ;
 * @endcode
 * 
 * @tparam Args definition of types of agrs
 * @param args user-supplied variables of basic types or arrays
 * @return std::string with produced JSON
 * 
 * @see tdaq::daq2soc::json2data() - a counterpart function to recreate C++ tuple of objects from JSON
 * @see test_json.cpp as example.
 */
template<typename ... Args>
std::string data2json(Args&&... args) {
	std::string ret {"["} ;
	internal::data2json(ret, std::forward<Args>(args)...) ;
	ret.append("]") ;
	return ret ;
}

template<typename ... Args>
payload_t data2json_v(Args&&... args) {
	payload_t ret; ret.emplace_back('[') ; // reserve?
	internal::data2json_v(ret, std::forward<Args>(args)...) ;
	ret.emplace_back(']') ; // need C++23 vector::emplace_range
	return ret ;
}

/**
 * @brief A wrapper for converting a tuple<Args...> into JSON
 * 
 * @tparam Args tuple construction arguments
 * @param tuple a tuple of basic types or arrays (C[] arrays or std::array)
 * @return std::string with produced JSON
 * 
 * @see test_json.cpp as example.
 */
template<typename ... Args>
std::string data2json(std::tuple<Args...> tuple) {
	// std::apply(internal::data2json, t) ?
	return internal::data2json_tuple(tuple, std::index_sequence_for<Args...>{}) ;
} 

//template<typename ... Args>
//payload_t data2json(std::tuple<Args...> tuple) {
	// std::apply(internal::data2json, t) ?
//	return internal::data2json_tuple_v(tuple, std::index_sequence_for<Args...>{}) ;
//}

/**
 * @brief A helper function to deserialize a JSON string into a tuple-like C++ data: a counter-part for data2json.
 * 
 * Converts JSON string into a "variadic" tuple of objects of user-supplied types (must correspond to the order of types used for creation of JSON),
 * or into single objects of the basic types or arrays.
 * It is expected that the string was created by data2json(). JSON array is decoded as std::array.
 * C++17 compiler support is required.
 * Example
 * @code
	MyData data_out { "counter A", 0, 0.0 } ; 
	auto mydata_as_json = tdaq::daq2soc::data2json(data_out) ;	// as tuple
	std::cerr << mydata_as_json << std::endl ; // or transfer it over http
	
	// get the sata as json string, deserialize it in the same type of tuple
	auto data_in = tdaq::daq2soc::json2data<std::string, int, double>(mydata_as_json) ; // use the same order of types

	// get fields from tuple by type (or by index)
	std::cerr << "Counter: " << std::get<std::string>(data_in) << "; int value: " << std::get<int>(data_in) << std::endl ;

 * @endcode
 *
 * @tparam DataObjects expected types of variables, or tuple<types...>
 * @param json JSON string like [ {"<type>": value}, [{obj}, ...], ...]
 * @return a tuple of objects of the types as defined by template parameter
 * 
 * @see test_json.cpp as example.
 * @see tdaq::daq2soc::data2json()
 */
template <typename... DataObjects>
auto json2data(const std::string_view& json) {
	return internal::Json2dataHelper<DataObjects...>::json2data(json) ;
}

}}

#endif
