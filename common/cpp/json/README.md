### JSON serialization library

A single-header library providing two functions: tdaq::daq2soc::data2json() to serialize a list of C++ variables (of basic types and arrays, or a tuple of those) into a JSON-formatted string like `[{"<type>": value}, ..., [{}, ...], ... ]`, which can then be passed from server to client (or vice-versa) so the latter can use the counter-part function tdaq::daq2soc::json2data() to instantiate a tuple of C++ objects in one line of code. See [example](test_json.cpp).  
C++17 support is required.  
Example:  

```cxx
using MyData = std::tuple<std::string, int, std::array<double, 4>> ;

MyData data_out { "counter A", 0, {0.0, 0.0, 0.0, 0.0} } ; 

auto mydata_json = tdaq::daq2soc::data2json(data_out) ;

std::cerr << mydata_json << std::endl ; // or transfer it over http with Sender API to server or back to client

// on the other side get the sata as json string, deserialize it in the same type of tuple
auto data_in = tdaq::daq2soc::json2data<MyData>(mydata_json) ; // use the same typedef

// get fields from the tuple by type (or by index)
std::cerr << "Counter: " << std::get<std::string>(data_in) << "; int value: " << std::get<int>(data_in) << std::endl ;
```

Doxygen:

[data2json](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/namespacetdaq_1_1daq2soc.html#a589d5dc4db58174088c444fd88838ddf)  
[json2data](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/namespacetdaq_1_1daq2soc.html#afb9664331cbdda7af4f285f6f2e63ae9)
