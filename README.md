### DAQ to SoC communication package - IMPLEMENTATION v 0.2.0

**Production branches are pushed to the official TDAQ phase 2 upgrade repositories group:**
https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/daq2soc

#### Branches and versions

~~[0.0.1](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/daq2soc/-/tree/0.0.1) - OBSOLETE a stable branch with first draft of the prototype (v 0.0.1)~~  
~~[0.1.0](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/daq2soc/-/tree/0.1.0) - OBSOLETE a prototype development branch (server based on nginx)~~

[0.2.0](https://gitlab.cern.ch/atlas-tdaq-ph2upgrades/daq2soc/-/tree/0.2.0) - current implementation branch. Changes w.r.t. prototype

[client](client/cpp/README.md):

- API changes (methods renamed) and extension with asyncrounous communication and timeouts
- API extention allowing to pass C++ tuples in request sending methods
- SSL support
- RHEL9 supported, CC7 obsolete

[server](server/README.md):

- no nginx binary, instead a prebuilt server binary and library are provided (boost/beast based) for `aarch64` platform
- API changes (classes renamed), extension for asynchronous publishing on user data on server
- SSL support (self-signed certificates)
- RHEL8/9 supported, CC7 is obsolete

#### Introduction

DAQ-to-SoC communication library allows a DAQ application (client) to exchange commands and data with a standalone (server) application running on SoC.

The implementation is based on `HTTP` protocol, which allows to send commands and payload to a server using `HTTP` POST requests, and get back a response including a response code plus arbitrary (text or binary) response body.

On the server side the implementation includes C++ code (implemented in a server application or dynamically loaded library) which serves requests on particular location like `http://server:port/tdaq/<endpoint>`, processes the requests and returns the result. Handling of `HTTP` communication is done by the library, to process the command request the user only needs to implement few virtual methods by inheriting from tdaq::daq2soc::DAQRequestProcessor C++ class.

On the client side, any `HTTP` library can be used to make `HTTP` requests to the SoC side. For C++, a higher-level API is provided (see tdaq::daq2soc::Sender class), hiding details of the `HTTP` exchange and also adding asynchronous level. The implementation is also based on `Boost/beast`, so the code is pretty common with the server part. A C++ API is provided with signatures similar to the ones on the server side, including synchronous and asynchronous access to the results of the calls. `Boost-1.75` is required, which is available in standard RHEL repos, and this is the only client-side requirement.

`HTTP`-based solution also makes it simple to communicate to the server from a web page, or from a command line scripts with `curl` utility (see some examples in [README.md](server/README.md)). It is also possible to access SoC systems hidden in a private network through a standard `HTTP/HTTPS` proxy (`apache`, `nginx`) running on a gateway Linux PC. For example, client code may use `SSL` version of the library for making `HTTPS` requests to a proxy, which forwards them to a SoC in plain `HTTP`.

Code is documented with [Doxygen](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/doxygen/0.2.0/).  
A single-file documentation also available as [PDF](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/daq2soc/0.2.0/refman.pdf).

#### Package content

##### client/cpp

C++ API files: header file, implementation, examples and a Makefile to compile it.

##### server

Makefile and sources for building and running `daq2soc_server` binary and a server shared library, and also user examples.

##### server/binaries/aarch64

Binaries of `daq2soc_server` server and `libdaq2soc_server.so` shared library compiled for `aarch64`. The binaries has no dependencies on externsal s/w and can run on any Linux ARM system with GLIBC 2.27+ (RHEL 8 and 9 families). Binaries are tested on Alma Linux 8.10 and 9.5, PetaLinux 2024.1 (Langdale). Users only need to provide a compiled library with a single user class, with no build time depdendencies other then daq2soc C++ headers.

The library can be used to build your own standalon server application (see an [example](server/examples/standalone_example_main.cpp)). The pre-built server needs to load synamically a user library (implementing tdaq::daq2soc::DAQRequestProcessor class interface). `libUserExample.so` is provided as an xample of such library.

SSL versions of the binaries have `_ssl` suffix in the file name.

##### server/examples

Source code for `libUserExample.so` and for a standalone user application using `daq2soc` as a library.

##### common/cpp/json

A single-header library implementing two functions: `data2json` and `json2data` for user data (tuples of basic types and arrays) (de)serialization to and from `JSON` string. Used in the server example for returning data (e.g. Histogram object) in `JSON` format. File `common/cpp/Histo.hpp` contains an example class which can be handled by this library.

##### quemu

Information and scripts to run a Linux distribution for `aarch64` platform virtually in Qemu VM running on a `x86_64` host.
